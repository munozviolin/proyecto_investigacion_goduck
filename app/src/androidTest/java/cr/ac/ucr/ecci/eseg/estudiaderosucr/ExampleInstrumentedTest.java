package cr.ac.ucr.ecci.eseg.estudiaderosucr;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.model.Reunion;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    private List<Reunion> listaPrueba;

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        assertEquals("cr.ac.ucr.ecci.eseg.estudiaderosucr", appContext.getPackageName());
    }

    @Test
    public void onDataChangeTest(){
    }
}
