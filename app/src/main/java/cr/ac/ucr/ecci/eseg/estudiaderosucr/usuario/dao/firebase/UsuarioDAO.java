package cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.dao.firebase;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model.Usuario;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.Debug;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.FireBaseHelper;

public class UsuarioDAO implements FireBaseHelper<Usuario> {

    private static final String USUARIOS = "usuarios";

    private DatabaseReference databaseReference;

    private MutableLiveData<List<Usuario>> usuariosMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<Usuario> usuarioMutableLiveData = new MutableLiveData<>();

    private Usuario usuarioPorID;

    public UsuarioDAO() {
        Debug.log();
        this.databaseReference = database.getReference(USUARIOS);
    }

    @Override
    public void agregar(final Usuario usuario) {
        Debug.log(usuario);
        databaseReference.child(usuario.getId()).setValue(usuario);
    }

    @Override
    public void eliminar(final Usuario usuario) {
        Debug.log(usuario);
        databaseReference.child(usuario.getId()).removeValue();
    }

    @Override
    public void cargarDatos() {
        Debug.log();
        final List<Usuario> usuarios = new ArrayList<>();
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                usuarios.clear();
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    usuarios.add(userSnapshot.getValue(Usuario.class));
                }
                usuariosMutableLiveData.setValue(usuarios);
                Debug.log(usuarios);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Debug.log(databaseError.toException());
            }
        });
    }

    @Override
    public void cargarDatosPorId(final String id) {
        Debug.log(id);
        databaseReference.child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Usuario usuario = dataSnapshot.getValue(Usuario.class);
                setUsuarioPorID(usuario);
                usuarioMutableLiveData.setValue(usuario);
                Debug.log(usuario);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Debug.log(databaseError.toException());
            }
        });
    }

    @Override
    public void modificar(final Usuario usuario) {
        // TODO: considerar que el usuario no existe
        Debug.log(usuario);
        databaseReference.child(usuario.getId()).setValue(usuario);
    }

    public LiveData<List<Usuario>> getUsuariosLiveData() {
        Debug.overlog(usuariosMutableLiveData.getValue());
        return usuariosMutableLiveData;
    }

    public LiveData<Usuario> getUsuarioLiveData() {
        Debug.overlog(usuarioMutableLiveData.getValue());
        return usuarioMutableLiveData;
    }

    //TODO: cambiar forma de utilización
    public Usuario getUsuarioPorID() {
        return usuarioPorID;
    }

    private void setUsuarioPorID(Usuario usuarioPorID) {
        this.usuarioPorID = usuarioPorID;
    }
}
