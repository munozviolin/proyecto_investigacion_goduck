package cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.model.Reunion;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model.Usuario;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "solicitud",
        foreignKeys = {
                @ForeignKey(entity = Usuario.class,
                        parentColumns = "id",
                        childColumns = "idUsuario"),
                @ForeignKey( entity = Reunion.class,
                        parentColumns = "id",
                        childColumns = "idReunion",
                        onDelete = CASCADE),
        }
)
public class Solicitud {

    public static final String SOLICITUD_ACEPTADA = "ACEPTADA";
    public static final String SOLICITUD_RECHAZADA = "RECHAZADA";
    public static final String SOLICITUD_PENDIENTE = "PENDIENTE";
    public static final String SIN_SOLICITUD = "SIN";


    @PrimaryKey
    @NonNull
    private String id;

    @ColumnInfo(name = "estado")
    private String estado;

    private String idReunion;

    private String idUsuario;

    public Solicitud(String id, String estado , String idReunion, String idUsuario) {
        this.id = id;
        this.estado = estado;
        this.idReunion = idReunion;
        this.idUsuario = idUsuario;
    }

    public Solicitud() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setEstado(String estado) {this.estado = estado;}

    public String getEstado() {return estado;}

    public String getIdReunion() {
        return idReunion;
    }

    public void setIdReunion(String idReunion) {
        this.idReunion = idReunion;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }
}
