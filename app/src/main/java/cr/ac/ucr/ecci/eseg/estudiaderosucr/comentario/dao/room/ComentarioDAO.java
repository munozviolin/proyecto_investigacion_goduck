package cr.ac.ucr.ecci.eseg.estudiaderosucr.comentario.dao.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Update;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.comentario.model.Comentario;

@Dao
public interface ComentarioDAO {

    @Insert
    void insert (Comentario... comentario);

    @Update
    void update(Comentario... comentario);

    @Delete
    void delete(Comentario... comentario);
}
