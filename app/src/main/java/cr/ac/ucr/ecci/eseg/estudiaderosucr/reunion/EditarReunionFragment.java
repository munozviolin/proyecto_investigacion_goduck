package cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.R;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.dao.firebase.ReunionesDAO;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.model.Reunion;

public class EditarReunionFragment extends Fragment implements View.OnClickListener {

    private Reunion reunion;

    private String curso;
    private String titulo;
    private String descripcion;
    private String fecha;
    private String hora;

    private Button buttonCancelarReunion;
    public Button buttonEditReunion;
    public EditText editTextNombreReunion;
    public EditText editTextDescripcion;

    private View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setTitulo("");
        setCurso("");
        setDescripcion("");
        setFecha("");
        setHora("");

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            reunion = bundle.getParcelable("reunion");
        }



        v = inflater.inflate(R.layout.fragment_editar_reunion, container, false);

        editTextDescripcion = v.findViewById(R.id.editTextDescripcion);
        editTextNombreReunion = v.findViewById(R.id.editTextNombreReunion);
        buttonEditReunion = v.findViewById(R.id.buttonEditReunion);
        buttonCancelarReunion = v.findViewById(R.id.buttonCancelarReunion);
        final AutoCompleteTextView autoCompleteCursosReunion =
                v.findViewById(R.id.autoCompleteCursosReunion);
        final EditText mDisplayDate = v.findViewById(R.id.editTextFechaReunion);
        final EditText mDisplayHour = v.findViewById(R.id.editTextHoraReunion);

        editTextDescripcion.setText(reunion.getDescripcion());

        editTextNombreReunion.setText(reunion.getTitulo());
        autoCompleteCursosReunion.setText(reunion.getCurso());

        String fechaP= reunion.getHoraInicio().split(" ")[0];
        String horaP= reunion.getHoraInicio().split(" ")[1]+" "+reunion.getHoraInicio().split(" ")[2];

        mDisplayDate.setText(fechaP);
        setFecha(fechaP);
        mDisplayHour.setText(horaP);
        setHora(horaP);






        final String[] cursos = getResources().getStringArray(R.array.cursos);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(requireContext(),
                android.R.layout.simple_list_item_1, cursos);
        autoCompleteCursosReunion.setAdapter(adapter);



        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                  int dayOfMonth) {
                                String fecha = String.valueOf(dayOfMonth) + "/" +
                                        String.valueOf(monthOfYear + 1) + "/" + String.valueOf(year);
                                mDisplayDate.setText(fecha);
                                setFecha(fecha);
                            }
                        }, year, month, day);
                datePicker.show();
            }
        });


        mDisplayHour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int hour = cal.get(Calendar.HOUR_OF_DAY);
                int minute = cal.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int pHour,
                                                  int pMinute) {
                                String ampmvalor = " am";
                                int horaFinal = pHour;
                                String minutoFinal = "" + pMinute;
                                if (pHour > 12) {
                                    horaFinal -= 12;
                                    ampmvalor = " pm";
                                } else if (pHour == 0) {
                                    horaFinal = 12;
                                }

                                if (pMinute < 10) {
                                    minutoFinal = "0" + pMinute;
                                }

                                String hora = horaFinal + ":" + minutoFinal + ampmvalor;
                                mDisplayHour.setText(hora);
                                setHora(hora);
                            }
                        }, hour, minute, false);
                timePickerDialog.show();
            }
        });

        buttonEditReunion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDescripcion(editTextDescripcion.getText().toString());
                setTitulo(editTextNombreReunion.getText().toString());
                setCurso(autoCompleteCursosReunion.getText().toString());

                //Si algún campo está vacío se le solicita al usuario que llene todos los espacios
                if (curso.equals("") || titulo.equals("") || descripcion.equals("") ||
                        hora.equals("") || fecha.equals("")) {
                    Toast.makeText(requireContext(), "Por favor llene todos los espacios",
                            Toast.LENGTH_LONG).show();
                } else {
                    List<String> cursosLista = Arrays.asList(cursos);
                    if (cursosLista.contains(curso)) {
                        abrirDialogo();
                    } else {
                        Toast.makeText(requireContext(), "El curso ingresado no pertenece a la " +
                                        "lista de \ncursos permitidos, por favor elija uno de la lista.",
                                Toast.LENGTH_LONG).show();
                    }
                }


            }
        });

        buttonCancelarReunion.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                cancelarReunion();
            }
        });

        return v;
    }

    /**
     * Cancela la reunión y redirige al mapa. Además, impide que el usuario pueda volver a acceder
     * a la vista editarReunión si se presiona el botón de retroceso.
     * Despliega un mensaje de confirmación
     */
    public void cancelarReunion()
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(
                getContext(), R.style.AlertDialogTheme));
        builder.setMessage("¿Desea Eliminar esta reunión?")
                .setPositiveButton("    Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        //Cancela la reunión
                        ReunionesDAO reunionesDAO = new ReunionesDAO();
                        reunionesDAO.eliminar(reunion);

                        //Indica que se ha eliminado la reunión
                        Toast.makeText(getContext(), "La reunión \"" + reunion.getTitulo() +
                                "\" ha sido eliminada", Toast.LENGTH_SHORT).show();

                        //Redigire al Fragmento Home
                        Navigation.findNavController(getParentFragment().getView())
                                .navigate(R.id.action_editarReunion_to_nav_home);

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id) {
                        // No hace nada
                }
            });
        builder.create();
        builder.show();
    }

    public void abrirDialogo() {
        final AlertDialog dialog = new AlertDialog.Builder(requireContext())
                .setTitle("Editar Reunión")
                .setTitle("¿Desea guardar estos cambios?")
                .setPositiveButton("Ok", null)
                .setNegativeButton("Cancelar", null)
                .show();

        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reunion.setTitulo(getTitulo());
                reunion.setCurso(getCurso());
                reunion.setDescripcion(getDescripcion());
                reunion.setHoraInicio(getFecha() + " " + getHora());

               ReunionesDAO reunionesDAO = new ReunionesDAO();
                reunionesDAO.modificar(reunion);
                Toast.makeText(requireContext(), "Cambios realizados a "+getTitulo(),
                       Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });

    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    @Override
    public void onClick(View view) {

    }
}
