package cr.ac.ucr.ecci.eseg.estudiaderosucr.login.ViewModel.data;

import androidx.annotation.Nullable;

import com.google.firebase.auth.FirebaseUser;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.Debug;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.ViewModelData;

/**
 * Representa el resultado de la autenticación.
 * Métodos de autenticación utilizados:
 * - Autenticación sin contraseña
 * <p>
 * Resultado correcto: almacena los datos del usuario de Firebase.
 * Resultado erroneo: almacena el id de un mensaje de error
 *
 * @author Daniel Marín M
 */
public class LoginResult implements ViewModelData<FirebaseUser> {

    @Nullable
    private FirebaseUser usuarioFirebase;
    @Nullable
    private Integer error;

    public LoginResult(@Nullable final FirebaseUser usuarioFirebase) {
        Debug.overlog(usuarioFirebase);
        setData(usuarioFirebase);
    }

    public LoginResult(@Nullable final Integer error) {
        Debug.overlog(error);
        setError(error);
    }

    @Override
    public void setData(@Nullable FirebaseUser usuarioFirebase) {
        Debug.overlog(usuarioFirebase);
        this.usuarioFirebase = usuarioFirebase;
        this.error = null;
    }

    @Override
    public void setError(@Nullable Integer error) {
        Debug.overlog(error);
        this.usuarioFirebase = null;
        this.error = error;
    }

    @Nullable
    @Override
    public FirebaseUser getData() {
        Debug.overlog(usuarioFirebase);
        return usuarioFirebase;
    }

    @Nullable
    @Override
    public Integer getError() {
        Debug.overlog(error);
        return error;
    }
}
