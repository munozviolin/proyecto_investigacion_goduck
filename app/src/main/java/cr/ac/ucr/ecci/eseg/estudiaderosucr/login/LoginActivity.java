package cr.ac.ucr.ecci.eseg.estudiaderosucr.login;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.transition.TransitionManager;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import java.util.UUID;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.R;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.login.ViewModel.LoginViewModel;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.login.ViewModel.LoginViewModelFactory;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.Debug;

/**
 * TODO: vincular google y checkear que existe el usuario de google para loguearse dado el caso
 * TODO: TRABAJAR Threads
 * TODO: revisar doble oncreate del activity
 *
 * @author Daniel Marín M
 */
public class LoginActivity extends AppCompatActivity {
    public static final int SPLASH_TIME_OUT = 2000;
    static final String CORREO_INSTITUCIONAL = "Correo institucional";
    private static final String UNIQUE_ID = "uuid";
    public static final String REGISTRO = "registro pendiente";
    public static final String USUARIO = "usuario";

    CredentialsSharedPreferences preferences;
    LoginViewModel viewModel;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_login);
        Debug.log();

        viewModel = new ViewModelProvider(this,
                new LoginViewModelFactory()).get(LoginViewModel.class);

        preferences = new CredentialsSharedPreferences(this);
        final String registro = preferences.cargarPreferencia(REGISTRO);

        Fragment fragment;
        if (registro == null) {
            fragment = LoginFragment.newInstance();
        } else {
            fragment = RegistroFragment.newInstance();
        }

        int delay = SPLASH_TIME_OUT;
        if (viewModel.esInicioMedianteEnlace(getIntent().getDataString())) delay = 0;

        final ConstraintLayout constraintLayout = findViewById(R.id.cl_act_login);
        final FragmentManager manager = getSupportFragmentManager();
        final FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.login_fragment_container, fragment);

        // retrasa el arranque y muestra el splash
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                TransitionManager.beginDelayedTransition(constraintLayout);
                if (!manager.isDestroyed()) {
                    transaction.commit();
                } else {
                    //TODO: revisar esto
                    Debug.log("ERROR EVITADO");
                }
            }
        }, delay);

        if (!viewModel.estanCargadosLosDatos()) {
            cargarPreferencias();
            setUUID();
            viewModel.setDatosCargados(true);
        }
    }

    private void cargarPreferencias() {
        String correo = preferences.cargarPreferencia(CORREO_INSTITUCIONAL);
        Debug.log(correo);
        if (correo != null) {
            viewModel.setCorreo(correo);
        }
    }

    private void setUUID() {
        String uuid = preferences.cargarPreferencia(UNIQUE_ID);
        Debug.log("id actual ", uuid);
        if (uuid == null) {
            uuid = UUID.randomUUID().toString();
            Debug.log("id nuevo ", uuid);
            preferences.guardarPreferencia(UNIQUE_ID, uuid);
        }
        viewModel.setUuid(uuid);
    }
}