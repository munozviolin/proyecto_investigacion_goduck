package cr.ac.ucr.ecci.eseg.estudiaderosucr.utils;

import android.text.Editable;
import android.text.TextWatcher;

public class TextChangedListener implements TextWatcher {

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //pass
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //pass
    }

    @Override
    public void afterTextChanged(Editable s) {
        //pass
    }
}
