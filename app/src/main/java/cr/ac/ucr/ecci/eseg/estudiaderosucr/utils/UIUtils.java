package cr.ac.ucr.ecci.eseg.estudiaderosucr.utils;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.android.material.snackbar.Snackbar;

public class UIUtils {

    public static void mostrarSnackbar(View view, String message) {
        View content = view.findViewById(android.R.id.content);
        Snackbar.make(content, message, Snackbar.LENGTH_SHORT).show();
    }

    public static void ocultarTeclado(Context context, View view) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
