package cr.ac.ucr.ecci.eseg.estudiaderosucr.utils;

import android.content.Context;


/**
 * Define los métodos a utilizar para los objetos que manejaran preferencias compartidas.
 *
 * @author Daniel Marín M
 */
public interface SharedPreferencesHelper<T> {

    String PERSISTIR = "persistir";

    void crearPreferencias(final Context context);

    void guardarPreferencia(final String llave, final T valor);

    T cargarPreferencia(final String llave);

    void BorrarPreferencia(final String llave);

    void eliminarPreferencias();

    void persistirPreferencias(final Boolean persistir);
}
