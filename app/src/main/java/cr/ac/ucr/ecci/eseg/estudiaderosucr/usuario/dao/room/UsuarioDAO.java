package cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.dao.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model.Usuario;

@Dao
public interface UsuarioDAO {

    @Insert
    void insert (Usuario... usuario);

    @Update
    void update(Usuario... usuario);

    @Delete
    void delete(Usuario... usuario);

    @Query("SELECT * FROM Usuario WHERE nombre LIKE :first AND " +
            "apellido LIKE :last LIMIT 1")
    Usuario findByName(String first, String last);
}