package cr.ac.ucr.ecci.eseg.estudiaderosucr.login.ViewModel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.Debug;


/**
 * Fabrica proveedora de ViewModel para instanciar un LoginViewModel.
 */
public class LoginViewModelFactory implements ViewModelProvider.Factory {

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(LoginViewModel.class)) {
            Debug.log("creando LoginViewModel");
            return (T) new LoginViewModel();
        } else {
            Debug.log("LoginViewModel no creado");
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}
