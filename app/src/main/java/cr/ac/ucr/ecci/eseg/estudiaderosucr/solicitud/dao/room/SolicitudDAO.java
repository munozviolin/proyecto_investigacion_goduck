package cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.dao.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Update;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.model.Solicitud;

@Dao
public interface SolicitudDAO {

    @Insert
    void insert(Solicitud... solicitud);

    @Update
    void update(Solicitud... solicitud);

    @Delete
    void delete(Solicitud... solicitud);
}
