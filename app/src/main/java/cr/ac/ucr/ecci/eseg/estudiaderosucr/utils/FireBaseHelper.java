package cr.ac.ucr.ecci.eseg.estudiaderosucr.utils;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Clase interfaz FireBaseHelper utilizada para compartir los metodos de interaccion
 * con la base de datos remota Firebase Real Time Database.
 * Pretende distribuir los metodos necesarios solo a aqullas clases que las requieran.
 *
 * @author Ignacio Vargas O.
 */
public interface FireBaseHelper<T> {

    FirebaseDatabase database = FirebaseDatabase.getInstance();

    void agregar(T objeto);

    void eliminar(T Objeto);

    /**
     * Metodo para cargar todos los datos existentes del
     * modelo utilizado para interfaz
     */
    void cargarDatos();

    /**
     * Metodo para obtener un dato o grupo de datos en especifico a partir de un ID
     *
     * @param id identificador del objeto que se quiere cargar
     */
    void cargarDatosPorId(String id);

    void modificar(T objetoNuevo);

}
