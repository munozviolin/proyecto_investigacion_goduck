package cr.ac.ucr.ecci.eseg.estudiaderosucr.database.room;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

public class DatabaseService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public DatabaseService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

    }


}
