package cr.ac.ucr.ecci.eseg.estudiaderosucr.login.ViewModel;

import android.util.Patterns;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.regex.Pattern;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.R;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.login.ViewModel.data.FormState;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.login.ViewModel.data.LoginResult;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.login.ViewModel.data.SentResult;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model.Usuario;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.repository.UserRepository;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.Debug;

/**
 * Se encarga de administrar los datos relacionados con el LoginActivity
 *
 * @author Daniel Marín M
 */
public class LoginViewModel extends ViewModel {

    private MutableLiveData<FormState> formStateMutableLiveData = new MutableLiveData<>();
    private LiveData<SentResult> sentResultLiveData;
    private LiveData<LoginResult> loginResultLiveData;
    private LiveData<Usuario> usuarioLiveData;

    private UserRepository userRepository;

    private FormState.LoginForm loginForm;
    private FormState.RegisterForm registerForm;

    private String uuid;
    private String correo;
    private String enlace;
    private Usuario nuevoUsuario;
    private boolean datosCargados;


    LoginViewModel() {
        Debug.log();
        userRepository = UserRepository.getInstance();
        sentResultLiveData = userRepository.getSentResult();
        loginResultLiveData = userRepository.getLoginResult();
        usuarioLiveData = userRepository.getUsuarioLiveData();
        uuid = null;
        datosCargados = false;
    }

    /**
     * Valida si el formulario cumple las reglas establecidas, y fija su estado como correcto o
     * incorrecto para activar o desactivar los botones de envío la UI.
     * Actualmente el formulario consta solo del correo institucional y se valida si cumple el
     * patrón {nombre}.{apellido}(@ucr.ac.cr)? o no.
     *
     * @param correoInstitucional correo institucional del usuario
     */
    public void validarFormulario(String correoInstitucional, boolean recordar) {
        Debug.log(correoInstitucional, recordar);
        String regex = "^[A-Za-z]+\\.[A-Za-z]+(@ucr\\.ac\\.cr)?$";
        loginForm = new FormState.LoginForm(correoInstitucional, recordar);
        if (correoInstitucional != null && Pattern.compile(regex).matcher(correoInstitucional).matches()) {
            Debug.overlog("El formulario es válido", correoInstitucional, recordar);
            formStateMutableLiveData.setValue(new FormState(loginForm));
        } else {
            Debug.overlog("El formulario no es válido", correoInstitucional, recordar);
            formStateMutableLiveData.setValue(new FormState(loginForm, R.string.invalid_email));
        }
    }

    public void validarFormulario(String nombre, String correo, String telefono) {
        Debug.log();
        String regexNombre = "^[A-ZÁÉÍÓÚÑ][a-záéíóúüñ]*(\\s[A-ZÁÉÍÓÚÑ][a-záéíóúüñ]*)+$";

        boolean nombreValido = Pattern.compile(regexNombre).matcher(nombre.trim()).matches();
        boolean correoValido = Patterns.EMAIL_ADDRESS.matcher(correo.trim()).matches() || correo.isEmpty();
        boolean telefonoValido = Patterns.PHONE.matcher(telefono.trim()).matches() || telefono.isEmpty();

        registerForm = new FormState.RegisterForm(nombre, correo, telefono);
        if (nombreValido && correoValido && telefonoValido) {
            Debug.overlog("El formulario es válido", nombre, correo, telefono);
            formStateMutableLiveData.setValue(new FormState(registerForm));
        } else {
            if (!nombreValido) {
                Debug.overlog("El nombre no es válido", nombre);
                formStateMutableLiveData.setValue(new FormState(registerForm, R.string.invalid_name));
            }
            if (!correoValido) {
                Debug.overlog("El correo no es válido", correo);
                formStateMutableLiveData.setValue(new FormState(registerForm, R.string.invalid_email));
            }
            if (!telefonoValido) {
                Debug.overlog("El telefono no es válido", telefono);
                formStateMutableLiveData.setValue(new FormState(registerForm, R.string.invalid_phone));
            }
        }
    }

    /**
     * Función wrapper de enviarEnlace de la clase UserRepository.
     *
     * @see UserRepository#enviarEnlace(String, String)
     */
    public void enviarEnlace(final String packageName, final String correoInstitucional) {
        String correo = concatenarDominio(correoInstitucional);
        Debug.overlog(correo);
        userRepository.enviarEnlace(packageName, correo);
    }

    /**
     * Función wrapper de esInicioMedianteEnlace de la clase UserRepository.
     *
     * @see UserRepository#esInicioMedianteEnlace(String)
     */
    public boolean esInicioMedianteEnlace(final String intentData) {
        boolean result = userRepository.esInicioMedianteEnlace(intentData);
        enlace = result ? intentData : null;
        Debug.overlog(result, enlace);
        return result;
    }

    /**
     * Función wrapper de ingresarPorEnlace de la clase UserRepository.
     *
     * @see UserRepository#ingresarPorEnlace(String, String)
     */
    public void ingresarPorEnlace(final String campo) {
        String correo = concatenarDominio(campo);
        Debug.overlog(correo, enlace);
        userRepository.ingresarPorEnlace(correo, enlace);
    }

    /**
     * Agrega el dominio ucr.ac.cr si campo del formulario solo contiene el nombre de usuario
     *
     * @param campo hilera que contiene un correo o el nombre de usuario
     * @return hilera con el correo institucional
     */
    private String concatenarDominio(String campo) {
        Debug.overlog(campo);
        return (campo.contains("@") ? campo : campo + "@ucr.ac.cr");
    }

    public Usuario crearNuevoUsuario(String correo) {
        nuevoUsuario = new Usuario();
        nuevoUsuario.setId(userRepository.getUid());
        correo = concatenarDominio(correo);
        nuevoUsuario.setCorreoInstitucional(correo);
        if (correo != null) {
            int iPunto = correo.indexOf("."), iArroba = correo.indexOf("@");
            String nombre = correo.substring(0, 1).toUpperCase() + correo.substring(1, iPunto);
            String apellido = correo.substring(iPunto + 1, iPunto + 2).toUpperCase() +
                    correo.substring(iPunto + 2, iArroba);
            nuevoUsuario.setNombre(nombre);
            nuevoUsuario.setApellido(apellido);
        }
        nuevoUsuario.setSesion_iniciada(true);
        nuevoUsuario.setUuid(uuid);
        return nuevoUsuario;
    }

    public void registrar() {
        Usuario usuario = getNuevoUsuario();
        String nombre = registerForm.getNombre();
        usuario.setNombre(nombre.substring(0, nombre.indexOf(" ")));
        usuario.setApellido(nombre.substring(nombre.indexOf(" ") + 1));
        usuario.setCorreoPersonal(registerForm.getCorreo());
        usuario.setTelefono(registerForm.getTelefono());
        Debug.log(usuario);
        userRepository.registrarCuenta(usuario);
    }

    public void cargarUsuarioActual() {
        Debug.overlog();
        userRepository.cargarUsuarioActual();
    }

    public void modificarUsuario(Usuario usuario) {
        usuario.setSesion_iniciada(true);
        usuario.setUuid(uuid);
        Debug.log(usuario);
        userRepository.modificarCuenta(usuario);
    }

    public void cancelarIngreso() {
        Debug.overlog();
        userRepository.cancelarIngreso();
    }

    public LiveData<FormState> getFormState() {
        Debug.overlog(formStateMutableLiveData);
        return formStateMutableLiveData;
    }

    public LiveData<SentResult> getSentResult() {
        Debug.overlog(sentResultLiveData);
        return sentResultLiveData;
    }

    public LiveData<LoginResult> getLoginResult() {
        Debug.overlog(loginResultLiveData);
        return loginResultLiveData;
    }

    public LiveData<Usuario> getUsuario() {
        Debug.overlog(usuarioLiveData);
        return usuarioLiveData;
    }

    public String getEtCorreoInstitucionalValue() {
        Debug.overlog(loginForm);
        return (loginForm != null) ? loginForm.getCorreo() : null;
    }

    public boolean getCbRecordarCredencialesValue() {
        Debug.overlog(loginForm);
        return (loginForm != null) && loginForm.isRecordar();
    }

    public String getUuid() {
        Debug.overlog(uuid);
        return uuid;
    }

    public void setUuid(String uuid) {
        Debug.overlog(uuid);
        this.uuid = uuid;
    }

    public String getCorreo() {
        Debug.overlog();
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Usuario getNuevoUsuario() {
        return nuevoUsuario;
    }

    public void setNuevoUsuario(Usuario nuevoUsuario) {
        this.nuevoUsuario = nuevoUsuario;
    }

    public boolean estanCargadosLosDatos() {
        Debug.overlog(datosCargados);
        return datosCargados;
    }

    public void setDatosCargados(boolean datosCargados) {
        Debug.overlog(datosCargados);
        this.datosCargados = datosCargados;
    }

    public String getUid(){
        return userRepository.getUid();
    }
}
