package cr.ac.ucr.ecci.eseg.estudiaderosucr.main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.BuildConfig;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.R;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.login.LoginActivity;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.main.ViewModel.MainViewModel;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.main.ViewModel.MainViewModelFactory;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model.Usuario;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.Debug;

import static cr.ac.ucr.ecci.eseg.estudiaderosucr.login.LoginActivity.USUARIO;
import static cr.ac.ucr.ecci.eseg.estudiaderosucr.login.LoginFragment.SALIR;


public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private MainViewModel mainViewModel;

    private TextView tvNombreUsuario;
    private TextView tvCorreoUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainViewModel = new ViewModelProvider(this, new MainViewModelFactory()).get(MainViewModel.class);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View navHeaderView = navigationView.getHeaderView(0);

        tvNombreUsuario = navHeaderView.findViewById(R.id.tv_nav_main_name);
        tvCorreoUsuario = navHeaderView.findViewById(R.id.tv_nav_main_email);

        cargarDatos();

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_perfil, R.id.nav_perfil)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        Menu menu = navigationView.getMenu();

        MenuItem cerrarSesion;
        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            switch (item.getItemId()) {
                case R.id.nav_perfil:
                case R.id.nav_home:
                    // Do something
                    break;
                case R.id.nav_logout:
                    item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            Debug.log();
                            mainViewModel.cerrarSesion();
                            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.putExtra(SALIR, true);
                            startActivity(intent);
                            finish();
                            return true;
                        }
                    });
                    break;
            }
        }
    }

    public void cargarDatos() {
        Intent intent = getIntent();
        Usuario usuario = intent.getParcelableExtra(USUARIO);
        if (usuario != null) {
            String nombre = usuario.getNombre() + " " + usuario.getApellido();
            tvNombreUsuario.setText(nombre);
            tvCorreoUsuario.setText(usuario.getCorreoInstitucional());
        } else if (BuildConfig.DEBUG) {
            Log.wtf("MAIN_ACTIVITY", "NO DEBERÍA DE PODER LLEGAR ACÁ SIN IDENTIFICARSE");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}