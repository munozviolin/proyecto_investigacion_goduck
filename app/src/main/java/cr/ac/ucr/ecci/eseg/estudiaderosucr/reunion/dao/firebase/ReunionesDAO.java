package cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.dao.firebase;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.model.Reunion;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.FireBaseHelper;

/**
 * Clase dedicada a los metodos utilizados por y para Reuniones y su interaccion con la base de datos
 *
 * @author Ignacio Vargas O.
 */
public class ReunionesDAO implements FireBaseHelper<Reunion> {

    private static final String REUNIONES = "reuniones";

    private List<Reunion> listaReuniones;

    public ReunionesDAO() {

    }

    @Override
    public void agregar(Reunion reunion) {

        DatabaseReference reference = database.getReference(REUNIONES);
        reference.child(reunion.getId()).setValue(reunion);
    }

    @Override
    public void eliminar(Reunion reunion) {
        DatabaseReference reference = database.getReference(REUNIONES);
        reference.child(reunion.getId()).removeValue();
    }

    @Override
    public void cargarDatos() {
        final List<Reunion> list = new ArrayList<>();
        DatabaseReference reference = database.getReference(REUNIONES);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {

                    list.add(data.getValue(Reunion.class));

                }
                setListaReuniones(list);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void cargarDatosPorId(String id) {

        //TODO
    }

    @Override
    public void modificar(Reunion objetoNuevo) {
        DatabaseReference reference = database.getReference(REUNIONES);
        reference = reference.child(objetoNuevo.getId());
        reference.setValue(objetoNuevo);
    }

    public List<Reunion> getReuniones() {
        return this.listaReuniones;
    }

    public void setListaReuniones(List<Reunion> listaReuniones) {
        this.listaReuniones = listaReuniones;
    }
}
