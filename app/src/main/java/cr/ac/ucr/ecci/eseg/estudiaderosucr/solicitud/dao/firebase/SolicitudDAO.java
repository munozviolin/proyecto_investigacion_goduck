package cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.dao.firebase;


import androidx.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.model.Solicitud;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model.Usuario;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.FireBaseHelper;

/**
 * Dao que contiene todas las operaciones relacionadas con la tabla solicitudes
 *
 * En el caso de los joins con la tabla usuario, se requieren seguir los siguientes pasos:
 * 1) Crear una nueva instancia de la listaUsuariosAsociadosSolicitudes
 * 2) Cada vez que se carga una nueva solicitud, se deben llamar al método obtenerUsuariodeSolicitud,
 * el cual se encargara de obtener el usuario asociado a la solicitud y de meterlo en la lista.
 *
 * Despues, se debe llamar al get de la lista de usuarios, con el fin de poder obtener la lista
 * de usuarios asociados a cada solicitud.
 * Este proceso debe realizarse al llamar a los siguientes métodos:
 *  cargarSolicitudesRevisadasPorReunion
 *  cargarSolicitudesPendientes
 *
 */

public class SolicitudDAO implements FireBaseHelper<Solicitud> {

    private static final String SOLICITUDES = "solicitudes";
    private static final String USUARIOS = "usuarios";
    private List<Solicitud> listaSolicitudes;
    private List<Solicitud> listaSolicitudesRevisadasReunion;
    private List<Solicitud> solicitudesPorReunion;
    private List<Usuario> listaUsuariosAsociadosSolicitudes;
    private Solicitud solicitudCargada;

    public SolicitudDAO() {
    }

    //agregar un comentario a la base de datos
    @Override
    public void agregar(Solicitud objeto) {
        //se obtiene el usuario actual de la aplicacion
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser  firebaseUser = firebaseAuth.getCurrentUser();
        //String userId = firebaseUser.getUid(); //id del usuario actual
        DatabaseReference mRef =  database.getReference(SOLICITUDES);

        mRef.child(objeto.getId()).setValue(objeto); //inserta la nueva tupla
    }

    //eliminar un comentario de la base de datos
    @Override
    public void eliminar(Solicitud solicitud) {
        DatabaseReference mRef =  database.getReference(SOLICITUDES);
        mRef.child(solicitud.getId()).removeValue(); //elimina la tupla con el id de solicitud respectivo
    }

    @Override
    public void cargarDatos() {

        final List<Solicitud> list = new ArrayList<>();
        DatabaseReference reference = database.getReference(SOLICITUDES);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot data: dataSnapshot.getChildren()) {

                    list.add(data.getValue(Solicitud.class));

                }
                setListaSolicitudes(list);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    /**
     * Carga todas las solicitudes que se encuentran en estado pendientes de una determinada
     * reunión.
     * @param idReunion El ID de la reunión
     */
    public void cargarSolicitudesPendientes(String idReunion) {

        final List<Solicitud> list = new ArrayList<>();
        listaUsuariosAsociadosSolicitudes = new ArrayList<>();

        DatabaseReference reference = database.getReference(SOLICITUDES);
        reference.orderByChild("idReunion").equalTo(idReunion).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot data: dataSnapshot.getChildren()) {

                     Solicitud nuevaSolicitud = data.getValue(Solicitud.class);

                    //Solamente despliega las solicitudes pendientes de la reunión
                    if(nuevaSolicitud.getEstado().equals((Solicitud.SOLICITUD_PENDIENTE)))
                    {
                        list.add(nuevaSolicitud);

                        //Agrega a la lista de usuarios la instancia del usuario asociado
                        //a dicha solicitud
                        obtenerUsuariodeSolicitud(nuevaSolicitud);
                    }

                }
                setSolicitudesPorReunion(list);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    /**
     * Almacena el usuario de la solicitud en una lista de solicitudes
     * @param solicitud
     */
    private void obtenerUsuariodeSolicitud(Solicitud solicitud)
    {

        DatabaseReference reference = database.getReference(USUARIOS);

        reference.orderByChild("id").equalTo(solicitud.getIdUsuario()).
                addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        for (DataSnapshot data : dataSnapshot.getChildren()) {
                            Usuario nuevoUsuario = data.getValue(Usuario.class);
                            listaUsuariosAsociadosSolicitudes.add(nuevoUsuario);


                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    /**
     * Método exclusivo de este dao. Carga la solicitudes a una reunión, excluyendo
     * las reuniones con estado pendiente
     * @param idReunion
     */
    public void cargarSolicitudesRevisadasPorReunion(String idReunion) {

        final List<Solicitud> list = new ArrayList<>();
        listaUsuariosAsociadosSolicitudes = new ArrayList<>();

        DatabaseReference reference = database.getReference(SOLICITUDES);

        reference.orderByChild("idReunion").equalTo(idReunion).
                addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        for (DataSnapshot data: dataSnapshot.getChildren()) {

                            Solicitud nuevaSolicitud = data.getValue(Solicitud.class);

                            if(!nuevaSolicitud.getEstado().equals(Solicitud.SOLICITUD_PENDIENTE)) {
                                list.add(nuevaSolicitud);

                                //Agrega a la lista de usuarios la instancia del usuario asociado
                                //a dicha solicitud
                                obtenerUsuariodeSolicitud(nuevaSolicitud);


                            }

                        }

                        //Al terminar de procesar las solicitudes, las almacena en la lista final
                        setListaSolicitudesRevisadasReunion(list);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }


    /**
     * Método exclusivo de este dao. Carga la solicitud de un usuario a una reunión en específico
     * Esto con el objetivo de obtener de que el usuario pueda saber si tiene una solicitud a
     * una reunión en específico.
     * @param idUsuario
     * @param idReunion
     */
    public void cargarSolicitudPorReunionUsuario(final String idUsuario, String idReunion) {

        DatabaseReference reference = database.getReference(SOLICITUDES);

        reference.orderByChild("idReunion").equalTo(idReunion).
                addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot data: dataSnapshot.getChildren()) {

                    Solicitud nuevaSolicitud = data.getValue(Solicitud.class);

                    if(nuevaSolicitud.getIdUsuario().equals((idUsuario)))
                        setSolicitudCargada(nuevaSolicitud);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void modificar(Solicitud s)
    {

    }

    public void modificarEstado(String solicitudId, String nuevoEstado) {
        DatabaseReference mRef =  database.getReference(SOLICITUDES);
        mRef.child(solicitudId).child("estado").setValue(nuevoEstado);
    }

    public Solicitud getSolicitudCargada() {return solicitudCargada;}

    public void setSolicitudCargada(Solicitud nuevaSolicitud)
    {
        solicitudCargada = nuevaSolicitud;
    }

    public List<Solicitud> getListaSolicitudes() {
        return listaSolicitudes;
    }

    public void setListaSolicitudes(List<Solicitud> listaSolicitudes) {
        this.listaSolicitudes = listaSolicitudes;
    }

    public List<Solicitud> getSolicitudesPorReunion() {
        return solicitudesPorReunion;
    }

    public void setSolicitudesPorReunion(List<Solicitud> solicitudesPorReunion) {
        this.solicitudesPorReunion = solicitudesPorReunion;
    }

    public List<Solicitud> getListaSolicitudesRevisadasReunion()
    {
        return this.listaSolicitudesRevisadasReunion;
    }

    public void setListaSolicitudesRevisadasReunion(List<Solicitud> listaSolicitudesRevisadasReunion)
    {
        this.listaSolicitudesRevisadasReunion = listaSolicitudesRevisadasReunion;
    }

    /**
     * Cuando se devuelven todas las solicitudes de una reunión, se debe traer por aparte una
     * lista que contenga todos los datos de los usuarios, de modo que se pueda saber el nombre
     * e imagen del usuario al que pertenece cada soliciud
     * @return
     */
    public List<Usuario> getListaUsuariosAsociadosSolicitudes()
    {
        return listaUsuariosAsociadosSolicitudes;
    }

    public void setListaUsuariosAsociadosSolicitudes(List<Usuario> listaUsuariosAsociadosSolicitudes)
    {
        this.listaUsuariosAsociadosSolicitudes = listaUsuariosAsociadosSolicitudes;
    }

    @Override
    public void cargarDatosPorId(String id)
    {

    }
}
