package cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.R;

public class VerUsuarioFragment extends Fragment {

    public static VerUsuarioFragment newInstance() {
        return new VerUsuarioFragment();
    }

    private TextView textoNombre;
    private TextView textoCorreo;
    private TextView textoTelefono;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.ver_usuario_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FirebaseUser usuario = FirebaseAuth.getInstance().getCurrentUser();
        textoNombre = requireActivity().findViewById(R.id.usuario_id_ver_perfil_texto);
        textoCorreo = requireActivity().findViewById(R.id.usuario_correo_ver_perfil_texto);
        textoTelefono = requireActivity().findViewById(R.id.usuario_telefono_ver_perfil_texto);

        textoNombre.setText(usuario.getDisplayName() );
        textoCorreo.setText(usuario.getEmail());
        textoTelefono.setText(usuario.getPhoneNumber());
    }
    //todo el view model obtiene un parcelable de user > guardar ese parcelable en un fragmento
    //todo acá es parcelable y pasarlo al view y después el view brinda los datos
}
