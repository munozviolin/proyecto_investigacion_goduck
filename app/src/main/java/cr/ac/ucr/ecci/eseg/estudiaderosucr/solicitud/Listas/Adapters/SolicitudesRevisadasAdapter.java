package cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.Listas.Adapters;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.R;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.dao.firebase.SolicitudDAO;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.model.Solicitud;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model.Usuario;


/**
 * Esta clase se encarga de configurar el adaptador personalizado de la lista principal.
 * Solamente se encarga de desplegar solicitudes aceptadas y rechazadas. Además, no permite
 * modificar el estado de la solicitud
 */

public class SolicitudesRevisadasAdapter extends BaseAdapter {



    private Context context;
    private List<Solicitud> listaSolicitudes; //Almacena el ID de los usuarios
    private List<Usuario> listaUsuarios;
    private SolicitudDAO solicitudDAO;


    public SolicitudesRevisadasAdapter(Context context, List<Solicitud> lista,
                                       List<Usuario> listaUsuarios,SolicitudDAO solicitudDAO)
    {
        this.context = context;
        this.listaSolicitudes = lista;
        this.listaUsuarios = listaUsuarios;
        this.solicitudDAO = solicitudDAO;



    }

    @Override
    public int getCount()
    {
        return listaSolicitudes.size();
    }

    @Override
    /**
     *  Es responsabilidad de la función llamadora instanciar el objeto Solicitud retornado
     */
    public Object getItem(int position)
    {
        return listaSolicitudes.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        //Pues no existe ningún mapeo de ID. El id de la lista es igual al id de la estructura
        //de datos que almacena las listas.
        return position;
    }

    public  void quitarElemento(int posicion)
    {
        listaSolicitudes.remove(posicion);
    }


    public View getView(final int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);

        final View rowView = inflater.inflate(R.layout.solicitudes_revisadas_list, parent, false);



        TextView nameText = (TextView) rowView.findViewById(R.id.name);
        TextView estadoText = (TextView) rowView.findViewById(R.id.estado);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.image);

        String estadoSolicitud = listaSolicitudes.get(position).getEstado();

        if(estadoSolicitud.equals(Solicitud.SOLICITUD_ACEPTADA))
            estadoText.setTextColor(context.getResources().getColor(R.color.AzulUCR));
        else
            estadoText.setTextColor(context.getResources().getColor(R.color.Café3UCR));

        estadoText.setText(estadoSolicitud);


        Usuario usuarioActual = listaUsuarios.get(position);
        nameText.setText(usuarioActual.getNombre() + " " + usuarioActual.getApellido());


        return rowView;

    }

}
