package cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.Listas.Fragmentos;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.R;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.dao.firebase.ReunionesDAO;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.model.Reunion;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.Listas.Adapters.SolicitudesPendientesAdapter;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.Listas.Adapters.SolicitudesRevisadasAdapter;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.dao.firebase.SolicitudDAO;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.model.Solicitud;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model.Usuario;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.repository.UserRepository;

/**
 * Este fragmento se encarga de desplegar una lista con todas las solicitudes procesadas de
 * una reunión en específico, la cual se determina según el ID de reunión recibido.
 *
 * Consiste en una lista que despliega cada solicitud, con la opción de aprobar o rechazar dicha
 * solicitud. Cada acción muestra un cuadro de confirmación
 */

public class SolicitudesProcesadasFragment extends Fragment {

    private String idReunion = "";
    private SolicitudDAO solicitudDao;
    private ListView listView;

    public SolicitudesProcesadasFragment() {

    }



    public static SolicitudesProcesadasFragment newInstance(String param1, String param2) {
        SolicitudesProcesadasFragment fragment = new SolicitudesProcesadasFragment();

        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_solicitudes_procesadas, container,
                false);

        listView = view.findViewById(R.id.solicitudes_revisadas_list);


        solicitudDao  = new SolicitudDAO();

        //Recibe el ID de la reunión por parámetro
        Bundle bundle = this.getArguments();


        if (bundle != null){
            idReunion = bundle.getString("ID","");
            solicitudDao.cargarSolicitudesRevisadasPorReunion(idReunion); //Carga todas las solicitutes pendientes
        }



        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                desplegarReuniones();
            }
        }, 1000);

        return view;
    }

    /**
     * Devuelve una lista con todos los ID de usuario que tienen una solicitud pendiente
     * con esta reunión
     * Todo: Cambiar ID por nombre de usuario e implementar imagenes
     * @return Lista con todos los ID de usuarios
     */
    private List<Solicitud> obtenerListaSolicitudesPendientes()
    {
        return solicitudDao.getListaSolicitudesRevisadasReunion();
    }

    private List<Usuario> obtenerListaUsuarios()
    {
        return solicitudDao.getListaUsuariosAsociadosSolicitudes();
    }

    private void desplegarReuniones()
    {


        SolicitudesRevisadasAdapter solicitudesAdapter = new SolicitudesRevisadasAdapter(getContext(),
                obtenerListaSolicitudesPendientes(), obtenerListaUsuarios(),solicitudDao);

        listView.setAdapter(solicitudesAdapter);

    }

}
