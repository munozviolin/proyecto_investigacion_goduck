package cr.ac.ucr.ecci.eseg.estudiaderosucr.utils;

import android.util.Log;

import androidx.annotation.NonNull;

import java.lang.reflect.Field;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.BuildConfig;

/**
 * Clase que maneja mensajes personalizados de depuración en el log, aquellos logs creados apartir
 * de esta clase serán deshabilitados idealmente en la aplicación release.
 * <p>
 * Se utiliza llamando a los métodos log u overlog con los argumentos adecuados (mensaje, objeto)
 *
 * @author Daniel Marín M
 */
public class Debug {

    // Manipular estas banderas al gusto

    // Activa en su totalidad los logs de esta clase
    private final static boolean DEBUG = true;

    // Muestra los tags como acrónimos
    private final static boolean ACRONYM = false;

    // Muestra la línea de código donde es llamado el log
    private final static boolean LOCATE = true;

    // Muestra los logs etiquetados como de sobra
    private final static boolean OVERLOG = true;

    // Muestra los contenidos del objeto pasado como parámetro al log
    private final static boolean CONTENTS = true;

    // Muestra los objetos android dentro de los objetos recibidos como parámetro
    private final static boolean ANDROID = false;

    // Profundidad en la que se muestran las variables miembro de los objetos
    private final static int DEPTH = 1;

    // Profundidad en la que se muestra el stack trace por llamado
    private final static int STACK = 2;


    // vanalidades
    private final static int SEP_LENGTH = 128;
    private final static String UPPER_SEP = "∨";
    private final static String LOWER_SEP = "∧";
    private final static String LOG_IDENTIFIER = "♠ ";
    private final static String OVERLOG_IDENTIFIER = " ♣";

    public static void log() {
        createLog("", false);
    }

    public static void overlog() {
        createLog("", true);
    }

    public static void log(String message) {
        createLog(message, false);
    }

    public static void overlog(String message) {
        createLog(message, true);
    }

    public static void log(Object... objects) {
        StringBuilder messageBuilder = new StringBuilder();
        if (CONTENTS) {
            for (Object object : objects) {
                messageBuilder.append(reflectObject(object, "", 0));
            }
        }
        createLog(messageBuilder.toString(), false);
    }

    public static void overlog(Object... objects) {
        StringBuilder messageBuilder = new StringBuilder();
        if (CONTENTS) {
            for (Object object : objects) {
                messageBuilder.append(reflectObject(object, "", 0));
            }
        }
        createLog(messageBuilder.toString(), true);
    }

    public static void log(String message, Object... objects) {
        if (CONTENTS && message != null) {
            StringBuilder messageBuilder = new StringBuilder(message);
            for (Object object : objects) {
                messageBuilder.append(reflectObject(object, "", 0));
            }
            message = messageBuilder.toString();
        }
        createLog(message, false);
    }

    public static void overlog(String message, Object... objects) {
        if (CONTENTS && message != null) {
            StringBuilder messageBuilder = new StringBuilder(message);
            for (Object object : objects) {
                messageBuilder.append(reflectObject(object, "", 0));
            }
            message = messageBuilder.toString();
        }
        createLog(message, true);
    }

    private static void createLog(String message, boolean isEvident) {
        if (DEBUG && BuildConfig.DEBUG && (OVERLOG || !isEvident)) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            StackTraceElement element = stackTrace[4];
            String tag = isEvident ? OVERLOG_IDENTIFIER : LOG_IDENTIFIER;
            String preMessage;
            if (ACRONYM) {
                StringBuilder tagBuilder = new StringBuilder();
                int lowercaseCount = 2;
                for (char c : element.getFileName().toCharArray()) {
                    if (c >= 'A' && c <= 'Z') {
                        tagBuilder.append(c);
                        lowercaseCount = 2;
                    } else if (lowercaseCount != 0 && !"aeiou".contains("" + c)) {
                        tagBuilder.append(c);
                        lowercaseCount--;
                    }
                }
                tag += tagBuilder.toString();
            } else {
                String fileName = element.getFileName();
                tag += fileName.substring(0, fileName.length() - 5);
            }

            StringBuilder preMessageBuilder = new StringBuilder();
            for (int i = 4 + (Math.max(STACK, 0)); i > 3; i--) {
                if (stackTrace.length > i) {
                    StackTraceElement e = stackTrace[i];
                    if (isCustomClass(e.getClassName())) {
                        if (i != 4) {
                            String fileName = e.getFileName();
                            preMessageBuilder.append(fileName.substring(0, fileName.length() - 5));
                            preMessageBuilder.append(".");
                        }
                        preMessageBuilder.append(e.getMethodName());
                        if (LOCATE) {
                            preMessageBuilder.append("[ln:");
                            preMessageBuilder.append(e.getLineNumber());
                            preMessageBuilder.append("]");
                        }
                        if (i == 4) {
                            preMessageBuilder.append(": ");
                        } else {
                            preMessageBuilder.append(" -> ");
                        }
                    }
                }
            }

            preMessage = preMessageBuilder.toString();
            Log.d(tag, preMessage + message);
        }
    }


    @NonNull
    private static String reflectObject(Object o, String name, int level) {
        if (BuildConfig.DEBUG) {
            StringBuilder reflection = new StringBuilder();

            if (o != null) {
                if (!isCustomClass(o)) {
                    reflection.append("{");
                    reflection.append(o.getClass().getSimpleName());
                    reflection.append(":");
                    reflection.append(o);
                    reflection.append("} ");
                } else {
                    if (SEP_LENGTH > 0) {
                        reflection.append("\n\t");
                        for (int i = 0; i < level; i++) reflection.append("\t");
                        for (int i = 0; i < SEP_LENGTH; i++) reflection.append(UPPER_SEP);
                    }
                    reflection.append("\n\t");
                    for (int i = 0; i < level; i++) reflection.append("\t");

                    reflection.append(o.getClass().getSimpleName());
                    reflection.append(" ");
                    reflection.append(name);
                    reflection.append(": ");

                    for (Field field : o.getClass().getDeclaredFields()) {
                        try {
                            field.setAccessible(true);
                            Object value = field.get(o);
                            if (value != null) {
                                boolean validLevel = level < DEPTH;
                                boolean isAndroidClass = value.toString().contains("android");
                                if (isCustomClass(value) && validLevel) {
                                    String fieldName = field.getName();
                                    String subR = reflectObject(value, fieldName, level + 1);
                                    reflection.append(subR);
                                } else if (ANDROID || !isAndroidClass) {
                                    reflection.append("\n\t\t");
                                    for (int i = 0; i < level; i++) reflection.append("\t");
                                    reflection.append(field.getType().getSimpleName());
                                    reflection.append(" ");
                                    reflection.append(field.getName());
                                    reflection.append(": ");
                                    reflection.append(value);
                                }
                            } else {
                                reflection.append("\n\t\t");
                                for (int i = 0; i < level; i++) reflection.append("\t");
                                reflection.append(field.getType().getSimpleName());
                                reflection.append(" ");
                                reflection.append(field.getName());
                                reflection.append(": null");
                            }
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                    if (SEP_LENGTH > 0) {
                        reflection.append("\n\t");
                        for (int i = 0; i < level; i++) reflection.append("\t");
                        for (int i = 0; i < SEP_LENGTH; i++) reflection.append(LOWER_SEP);
                    }
                }
            } else {
                reflection.append("null");
            }

            return reflection.toString();
        }
        return "";
    }

    private static boolean isCustomClass(Object o) {
        boolean isPrimitive = o.getClass().isPrimitive();
        return !isPrimitive && isCustomClass(o.getClass().getName());
    }

    private static boolean isCustomClass(String name) {
        boolean isJavaClass = name.contains("java.lang");
        boolean isAndroidClass = name.contains("android");
        boolean isGoogleClass = name.contains("google");
        return !isJavaClass && !isAndroidClass && !isGoogleClass;
    }
}
