package cr.ac.ucr.ecci.eseg.estudiaderosucr.database.room.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.model.Reunion;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model.Usuario;

@Entity(tableName = "usuario_reunion_join",
        primaryKeys = { "idUsuario", "idReunion" },
        foreignKeys = {
                @ForeignKey(entity = Usuario.class,
                        parentColumns = "id",
                        childColumns = "idUsuario"),
                @ForeignKey(entity = Reunion.class,
                        parentColumns = "id",
                        childColumns = "idReunion")
        })
public class  UsuariosReunionJoin {
    @NonNull
    public final String idUsuario;
    @NonNull
    public final String idReunion;


    public UsuariosReunionJoin(@NonNull String idUsuario, @NonNull String idReunion) {
        this.idUsuario = idUsuario;
        this.idReunion = idReunion;
    }
}
