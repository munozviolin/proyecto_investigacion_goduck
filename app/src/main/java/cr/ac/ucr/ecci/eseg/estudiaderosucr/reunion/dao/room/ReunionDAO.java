package cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.dao.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Update;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.model.Reunion;

@Dao
public interface ReunionDAO {

    @Insert
    void insert (Reunion... reunion);

    @Update
    void update(Reunion... reunion);

    @Delete
    void delete(Reunion... reunion);
}
