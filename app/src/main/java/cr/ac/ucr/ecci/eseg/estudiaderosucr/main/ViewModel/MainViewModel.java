package cr.ac.ucr.ecci.eseg.estudiaderosucr.main.ViewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model.Usuario;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.repository.UserRepository;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.Debug;

public class MainViewModel extends ViewModel {

    private LiveData<Usuario> usuarioLiveData;

    private UserRepository userRepository;

    MainViewModel() {
        Debug.log();
        userRepository = UserRepository.getInstance();
        usuarioLiveData = userRepository.getUsuarioLiveData();
        userRepository.cargarUsuarioActual();
    }

    public void cerrarSesion(){
        Usuario usuario = usuarioLiveData.getValue();
        userRepository.cerrarSesion(usuario);
    }
}
