package cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.Listas.Adapters;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.R;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.dao.firebase.SolicitudDAO;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.model.Solicitud;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model.Usuario;


/**
 * Esta clase se encarga de configurar el adaptador personalizado de la lista principal.
 * Almacena la lista de todas las tabletop, de modo que pueda almacenar el nombre y descripción
 */

public class SolicitudesPendientesAdapter extends BaseAdapter {

    //Constantes utilizadas para saber que tipo de fila dibujar
    public final  int ACEPTADA = 0;
    public final  int PENDIENTE = 1;
    public final  int RECHAZADA = 2;


    private Context context;
    private List<Solicitud> listaSolicitudes; //Almacena el ID de los usuarios
    private List<Usuario> listaUsuarios;
    private SolicitudDAO solicitudDAO;

    private List<Integer> estadoSolicitudes;

    public SolicitudesPendientesAdapter(Context context, List<Solicitud> lista,
                                        List<Usuario> listaUsuarios, SolicitudDAO solicitudDAO)
    {
        this.context = context;
        this.listaSolicitudes = lista;
        this.listaUsuarios = listaUsuarios;
        this.solicitudDAO = solicitudDAO;

        //Inicializa la lista de estado de solicitudes, y asigna a cada una el estado pendiente
        estadoSolicitudes = new ArrayList<>();

        for(int solicitud = 0; solicitud < listaSolicitudes.size(); ++solicitud)
        {
            estadoSolicitudes.add(PENDIENTE);
        }



    }

    @Override
    public int getCount()
    {
        return listaSolicitudes.size();
    }

    @Override
    /**
     *  Es responsabilidad de la función llamadora instanciar el objeto Solicitud retornado
     */
    public Object getItem(int position)
    {
        return listaSolicitudes.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        //Pues no existe ningún mapeo de ID. El id de la lista es igual al id de la estructura
        //de datos que almacena las listas.
        return position;
    }

    public  void quitarElemento(int posicion)
    {
        listaSolicitudes.remove(posicion);
    }

    //Muestra en la lista que la solicitud de la fila correspondiente ya se aprobó
    public void cambiarVistaAprobado(Context context, View rowView)
    {
        rowView.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        rowView.findViewById(R.id.image).setVisibility(View.INVISIBLE);
        rowView.findViewById(R.id.acceptView).setVisibility(View.INVISIBLE);
        rowView.findViewById(R.id.rejectView).setVisibility(View.INVISIBLE);

        TextView campoNombre = (TextView)rowView.findViewById(R.id.name);
        campoNombre.setText("Solicitud Aprobada");
        campoNombre.setTextColor(context.getResources().getColor(R.color.Blanco));
    }

    //Muestra en la lista que la solicitud de la fila correspondiente ya se rechazo
    public void cambiarVistaRechazado(Context context, View rowView)
    {
        rowView.setBackgroundColor(context.getResources().getColor(R.color.Café3UCR));
        rowView.findViewById(R.id.image).setVisibility(View.INVISIBLE);
        rowView.findViewById(R.id.acceptView).setVisibility(View.INVISIBLE);
        rowView.findViewById(R.id.rejectView).setVisibility(View.INVISIBLE);

        TextView campoNombre = (TextView)rowView.findViewById(R.id.name);
        campoNombre.setText("Solicitud Rechazada");
        campoNombre.setTextColor(context.getResources().getColor(R.color.Blanco));
    }

    public void aceptarSolicitud(Context context, View rowView, int posicion)
    {

        estadoSolicitudes.set(posicion,ACEPTADA);
        cambiarVistaAprobado(context,rowView);

    }

    public void rechazarSolicitud(Context context, View rowView, int posicion)
    {
        estadoSolicitudes.set(posicion,RECHAZADA);
        cambiarVistaRechazado(context,rowView);
    }

    public View getView(final int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);

        final View rowView = inflater.inflate(R.layout.solicitudes_list, parent, false);



        TextView nameText = (TextView) rowView.findViewById(R.id.name);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.image);
        ImageView acceptView = (ImageView) rowView.findViewById(R.id.acceptView);
        ImageView rejectView = (ImageView) rowView.findViewById(R.id.rejectView);


        Usuario usuarioActual = listaUsuarios.get(position);
        final String nombreUsuario =  usuarioActual.getNombre() + " " + usuarioActual.getApellido();
        nameText.setText(nombreUsuario);

        int estadoSolicitudActual = estadoSolicitudes.get(position);

        if(estadoSolicitudActual == PENDIENTE) {
            //Setea los image view para que muestren el respectivo mensaje de confirmación
            acceptView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(
                            context, R.style.AlertDialogTheme));
                    builder.setMessage("¿Desea aceptar la solicitud de "+nombreUsuario+"?")
                            .setPositiveButton("    Aceptar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //Realiza el cambio necesario en la base de datos
                                    solicitudDAO.modificarEstado(listaSolicitudes.get(position).getId(),
                                            Solicitud.SOLICITUD_ACEPTADA);
                                    aceptarSolicitud(context, rowView, position);
                                }
                            })
                            .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                }
                            });
                    // Create the AlertDialog object and return it
                    builder.create();
                    builder.show();


                }
            });

            rejectView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(
                            context, R.style.AlertDialogTheme));
                    builder.setMessage("¿Desea rechazar la solicitud de "+nombreUsuario+"?")
                            .setPositiveButton("    Aceptar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    solicitudDAO.modificarEstado(listaSolicitudes.get(position).getId(),
                                            Solicitud.SOLICITUD_RECHAZADA );
                                    rechazarSolicitud(context, rowView, position);
                                }
                            })
                            .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                }
                            });
                    // Create the AlertDialog object and return it
                    builder.create();
                    builder.show();
                }
            });
            //Todo Implementar imagenes de perfil cuando eso exista
        }
        else if(estadoSolicitudActual == ACEPTADA)
        {
            cambiarVistaAprobado(context,rowView);
        }
        else if(estadoSolicitudActual == RECHAZADA)
        {
            cambiarVistaRechazado(context,rowView);
        }

        return rowView;

    }

}
