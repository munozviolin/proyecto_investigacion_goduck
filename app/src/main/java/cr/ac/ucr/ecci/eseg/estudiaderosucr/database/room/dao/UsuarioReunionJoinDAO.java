package cr.ac.ucr.ecci.eseg.estudiaderosucr.database.room.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.database.room.model.UsuariosReunionJoin;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model.Usuario;

@Dao
public interface UsuarioReunionJoinDAO {
    @Insert
    void insert(UsuariosReunionJoin usuariosReunionJoin);

    @Query("SELECT * FROM usuario INNER JOIN usuario_reunion_join ON " +
            "usuario.id=usuario_reunion_join.idUsuario " +
            "WHERE usuario_reunion_join.idReunion=:idReunion")
    List<Usuario> getUsuariosPorReunion(final String idReunion);
}