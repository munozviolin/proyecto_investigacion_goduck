package cr.ac.ucr.ecci.eseg.estudiaderosucr.login.ViewModel.data;

import androidx.annotation.Nullable;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.Debug;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.ViewModelData;

/**
 * Representa el resultado de haber enviado el correo electrónico correctamente:
 * <p>
 * Resultado correcto: almacena el correo electrónico del usuario.
 * Resultado erroneo: almacena el id de un mensaje de error.
 * <p>
 * El dominio ucr.ac.cr acepta correos electrónicos dirigidos a cualquier correo perteneciente a
 * este, es decir se puede enviar un correo a una dirección aleatoria y el dominió lo acepta.
 *
 * @author Daniel Marín M
 */
public class SentResult implements ViewModelData<String> {

    @Nullable
    private String correoInstitucional;
    @Nullable
    private Integer error;

    public SentResult(@Nullable final String correoInstitucional) {
        Debug.overlog(correoInstitucional);
        setData(correoInstitucional);
    }

    public SentResult(@Nullable final Integer error) {
        Debug.overlog(error);
        setError(error);
    }

    @Override
    public void setData(@Nullable String correoInstitucional) {
        Debug.overlog(correoInstitucional);
        this.correoInstitucional = correoInstitucional;
        this.error = null;
    }

    @Override
    public void setError(@Nullable Integer error) {
        Debug.overlog(error);
        this.correoInstitucional = null;
        this.error = error;
    }

    @Nullable
    @Override
    public String getData() {
        Debug.overlog(correoInstitucional);
        return correoInstitucional;
    }

    @Nullable
    @Override
    public Integer getError() {
        Debug.overlog(error);
        return error;
    }
}
