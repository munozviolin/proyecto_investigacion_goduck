package cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.Listas.Adapters;

import android.os.Bundle;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;


import java.util.List;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.Listas.Fragmentos.SolicitudesPendientesFragment;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.Listas.Fragmentos.SolicitudesProcesadasFragment;

/**
 * Adaptador para determinar cuales fragmentos seran desplegados en el PageViewer del fragmento
 * SolicitudesReunion.
 * Además, permite mapear cada pantalla con un título en el TabLayout
 *
 */
public class SolicitudesPageAdapter extends FragmentStatePagerAdapter {

    public final int CANTIDAD_PAGINAS = 2;
    public final int FRAGMENTO_SOLICITUDES_PENDIENTES = 0;
    public final int FRAGMENTO_SOLICITUDES_REVISADAS = 1;
    private List<Fragment> listaPantallas;
    private List<String> listaTituloPantallas;
    private String idReunion;



    /**
     * Crea una nueva instancia del adaptador de páginas de solicitudes
     * @param fragmentManager un childFragmentManager para remplazo entre fragmentos
     * @param listaPantallas Una lista en donde cada elemento es un fragmento de un listado de
     *                       solicitudes
     * @param listaTituloPantallas una lista en donde cada elemento es el titulo de su respectiva
     *                             pantalla. Este título aparece en el tab correspondiente a la
     *                             pantalla.
     * @param idReunion  El id de la reunión desde la cual se consultaran las solicitudes pendientes
     *                   y revisadas
     */
    public SolicitudesPageAdapter(FragmentManager fragmentManager, List<Fragment> listaPantallas,
                                  List<String> listaTituloPantallas, String idReunion)
    {
        super(fragmentManager);
        this.listaPantallas = listaPantallas;
        this.listaTituloPantallas = listaTituloPantallas;
        this.idReunion = idReunion;
    }

    @Override
    public int getCount()
    {
        return CANTIDAD_PAGINAS;
    }

    @Override
    public Fragment getItem(int posicion)
    {
        Fragment newFragmentInstance = listaPantallas.get(posicion);

        //Carga un nuevo bundle para enviar el id de la reunion, para el nuevo fragmento
        Bundle parametros = new Bundle();
        parametros.putString("ID",idReunion);

        /**
         * Dependiendo de la posición del fragmento se crea una nueva instancia del tipo de
         * fragmento a desplegar en el TabView
         */
        switch(posicion)
        {
            case FRAGMENTO_SOLICITUDES_PENDIENTES:
                newFragmentInstance = new SolicitudesPendientesFragment();
                break;

            case FRAGMENTO_SOLICITUDES_REVISADAS:
                newFragmentInstance = new SolicitudesProcesadasFragment();
                break;
        }
        newFragmentInstance.setArguments(parametros);
        return newFragmentInstance;
    }


    /**
     * Permite reiniciar el fragmento cada vez que se haga un tab switcing
     * @param object
     * @return
     */
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }




    @Nullable
    @Override
    public CharSequence getPageTitle(int posicion) {
        return listaTituloPantallas.get(posicion);
    }
}
