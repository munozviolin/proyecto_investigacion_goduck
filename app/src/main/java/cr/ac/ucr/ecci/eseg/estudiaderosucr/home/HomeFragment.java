package cr.ac.ucr.ecci.eseg.estudiaderosucr.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.DrawableRes;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.comentario.dao.firebase.ComentarioDAO;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.comentario.model.Comentario;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.model.Reunion;

import androidx.navigation.Navigation;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.dao.firebase.ReunionesDAO;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.R;

public class HomeFragment extends Fragment implements OnMapReadyCallback,
        GoogleMap.OnMyLocationClickListener, GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener {

    private final int REQUEST_LOCATION_PERMISSION = 1; //verificacion de permiso de ubicacion
    private final int milisegundosEspera = 3500; //tiempo de espera para cargar
    private FusedLocationProviderClient mFusedLocationProviderClient; //servira para obtener actualizaciones de ubicacion del dispositivo

    private final double initialLatitude = 9.936989;
    private final double initialLongitude = -84.051022;

    private final double latitudeSouthwestBound = 9.934870;
    private final double longitudeSouthwestBound = -84.055248;
    private final double latitudeNortheastBound = 9.941898;
    private final double longitudeNortheastBound = -84.043427;

    private List<Marker> listaMarcadores;
    private MenuItem lupa;
    private MenuItem cancelar;
    private String curso_filtrado_seleccionado;
    private AutoCompleteTextView curso_filtrado;

    private final LatLngBounds UCR = new LatLngBounds(
            new LatLng(latitudeSouthwestBound, longitudeSouthwestBound),
            new LatLng(latitudeNortheastBound, longitudeNortheastBound));

    protected Location mLastKnownLocation;
    private GoogleMap map;
    private ReunionesDAO reunionModel;
    private View createdView;
    private FloatingActionButton fab;
    private Snackbar creatingReunionSnackbar;

    public HomeFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();

        String[] cursos = getResources().getStringArray(R.array.cursos);
        curso_filtrado = requireActivity().findViewById(R.id.autoCompleteTextViewCursos);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireActivity(),
                android.R.layout.simple_list_item_1, cursos);
        curso_filtrado.setAdapter(adapter);
        setActionSearchListeners();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        //Empieza la carga de datos de todas las reuniones
        reunionModel = new ReunionesDAO();
        listaMarcadores = new LinkedList<>();
        setHasOptionsMenu(true);


        createdView = inflater.inflate(R.layout.fragment_home, container, false);

        fab = createdView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                allowCreateReunion();
            }
        });


        return createdView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            SupportMapFragment mapaFragment = (SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map);
            if (mapaFragment != null) {
                mapaFragment.getMapAsync(this);
            }
        }
    }

    @Override
    public boolean onMarkerClick(final Marker marcador) {
        return false;
    }

    @Override
    public void onInfoWindowClick(final Marker marker) {
        Bundle parameter = new Bundle();
        Reunion reunion = (Reunion) marker.getTag();
        parameter.putString("ID", "" + reunion.getId());
        Navigation.findNavController(getParentFragment().getView()).navigate(R.id.action_nav_home_to_reunionFragment, parameter);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        map = googleMap; //instancia del mapa
        requestLocationPermission();
        final int minZoomLevel = 18;
        LatLng initialLocation = new LatLng(initialLatitude, initialLongitude); //localizacion inicial de la camara del mapa
        map.moveCamera(CameraUpdateFactory.newLatLng(initialLocation)); //mover la camara a la localizacion inicial
        map.setLatLngBoundsForCameraTarget(UCR); //se aplican los limites al mapa que podran visualizarse
        map.setMinZoomPreference(minZoomLevel); //zoom inicial de la vista del mapa
        map.setOnMarkerClickListener(this);
        map.setOnInfoWindowClickListener(this);
        this.reunionModel.cargarDatos();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                createCustomMarker(map);
            }
        }, milisegundosEspera);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @SuppressLint("MissingPermission")
    @AfterPermissionGranted(REQUEST_LOCATION_PERMISSION)
    public void requestLocationPermission() {
        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION}; //permisos que se comprobaran
        if (EasyPermissions.hasPermissions(requireContext(), perms)) {
            mFusedLocationProviderClient = LocationServices
                    .getFusedLocationProviderClient(requireContext());
            map.setMyLocationEnabled(true);
            map.setOnMyLocationButtonClickListener(this);
            map.setOnMyLocationClickListener(this);
        } else {
            EasyPermissions.requestPermissions(this,
                    getResources().getString(R.string.autorizar_permisos),
                    REQUEST_LOCATION_PERMISSION, perms);
        }
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
    }

    @Override
    public boolean onMyLocationButtonClick() {
        final LatLng ubicacionInicial = new LatLng(initialLatitude, initialLongitude); //ubicacion con latitud y longitud iniciales

        Task actualizacionUbicacion = mFusedLocationProviderClient.getLastLocation();
        final float zoomLevel = map.getCameraPosition().zoom; //nivel zoom actual
        actualizacionUbicacion.addOnCompleteListener(requireActivity(), new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()) {
                    mLastKnownLocation = (Location) task.getResult(); // busqueda de la ultima ubicacion conocida
                    if (mLastKnownLocation != null) {

                        LatLng newLocation = new LatLng(mLastKnownLocation.getLatitude(),
                                mLastKnownLocation.getLongitude());

                        //Verifica que el mapa no se mueva fuera de la zona límite establecidas
                        if (UCR.contains(newLocation)) {
                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(newLocation, zoomLevel));

                            Toast.makeText(requireContext(), getResources().getString(R.string.ubicacion_actualizada),
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(requireContext(), getResources().getString(R.string.ubicacion_fuera_limites)
                                    , Toast.LENGTH_LONG).show();
                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(initialLatitude,
                                    initialLongitude), zoomLevel));
                        }
                    } else {
                        Toast.makeText(requireContext(), getResources().getString(R.string.ubicacion_desactivada),
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(ubicacionInicial, zoomLevel));
                    map.getUiSettings().setMyLocationButtonEnabled(false);
                }
            }
        });
        return false;
    }

    /**
     * Crea un marcador de una reunión en el mapa
     *
     * @param googleMap Instancia del mapa
     * @param id        Id de la reunión
     * @param title     Titulo de la reunión
     * @param latitude  Latitud de la reunión
     * @param longitude Longitud de la reunión
     */
    private void agregarMarcadorPersonalizado(GoogleMap googleMap, String id, String title, double latitude,
                                              double longitude, String curso) {
        //Permite almacenar solo la información relevante para no saturar la memoria
        Reunion reunionMarcador = new Reunion();
        reunionMarcador.setCurso(curso);
        reunionMarcador.setId(id);

        try {
            // para agregar un marcador con la imagen personalizada
            Marker marker = googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude))
                    .title(title)
                    .icon(BitmapDescriptorFactory.fromBitmap(getImagenMarcador(R.drawable.marker))));

            marker.setTag(reunionMarcador);
            listaMarcadores.add(marker);
        } catch (IllegalStateException e) {

        }
    }

    /**
     * Se encarga de crear un marcador por cada reunión presente en la base de datos
     * Se recomienda esperar un tiempo prudencial antes de llamar a este método, para darle tiempo
     * a Firebase de traerse los datos
     * TODO: Implementar un servicio que llame a este método
     *
     * @param googleMap Instancia del mapa de google
     */
    private void createCustomMarker(GoogleMap googleMap) {
        List<Reunion> reuniones = this.reunionModel.getReuniones();

        if (reuniones != null) {
            for (Reunion reunion : reuniones) {

                if (UCR.contains(new LatLng(reunion.getLatitud(), reunion.getLongitud()))) {
                    agregarMarcadorPersonalizado(googleMap, reunion.getId(), reunion.getTitulo(),
                            reunion.getLatitud(), reunion.getLongitud(), reunion.getCurso());
                }
            }
        } else {
            Toast.makeText(requireContext(), getResources().getString(R.string.error_despliegue_marcadores)
                    , Toast.LENGTH_LONG).show();
        }
    }

    private Bitmap getImagenMarcador(@DrawableRes int id) throws IllegalStateException {
        View marcadorPersonalizado = ((LayoutInflater)
                requireContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);
        ImageView imagen = (ImageView) marcadorPersonalizado.findViewById(R.id.profile_image);
        imagen.setImageResource(id);
        marcadorPersonalizado.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        marcadorPersonalizado.layout(0, 0, marcadorPersonalizado.getMeasuredWidth(), marcadorPersonalizado.getMeasuredHeight());
        marcadorPersonalizado.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(marcadorPersonalizado.getMeasuredWidth(), marcadorPersonalizado.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = marcadorPersonalizado.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        marcadorPersonalizado.draw(canvas);
        return bitmap;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        lupa = menu.getItem(0);
        lupa.setVisible(true);
        cancelar = menu.getItem(1);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                if (curso_filtrado.getVisibility() == View.GONE) {
                    curso_filtrado.setVisibility(View.VISIBLE);
                    lupa.setVisible(false);
                    cancelar.setVisible(true);
                } else {
                    iniciarBusqueda(curso_filtrado);
                }
                return true;
            case R.id.action_settings:
                return true;
            case R.id.action_cancel:
                cancelarBusqueda();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void iniciarBusqueda(AutoCompleteTextView view) {
        cancelar.setVisible(true);
        curso_filtrado_seleccionado = view.getText().toString();
        if (filtrarMarcadores()) {
            lupa.setVisible(false);
        } else {
            cancelarBusqueda();
        }
        ocultarElementosBusqueda(view);
    }

    private void cancelarBusqueda() {
        for (Marker m : listaMarcadores) {
            m.setVisible(true);
        }
        cancelar.setVisible(false);
        lupa.setVisible(true);
        ocultarElementosBusqueda(curso_filtrado);
    }

    private void ocultarElementosBusqueda(AutoCompleteTextView view) {
        ocultarTeclado(view);
        view.setText("");
        view.setVisibility(View.GONE);
    }

    public boolean filtrarMarcadores() {
        boolean existeReunion = false;
        String curso;
        for (Marker m : listaMarcadores) {
            Reunion reunion = (Reunion) m.getTag();
            curso = reunion.getCurso();
            if (curso.equals(curso_filtrado_seleccionado)) {
                m.setVisible(true);
                existeReunion = true;
            } else
                m.setVisible(false);
        }
        if (!existeReunion)
            Snackbar.make(requireActivity().findViewById(android.R.id.content),
                    "No existen reuniones creadas para este curso", Snackbar.LENGTH_SHORT).show();
        return existeReunion;
    }

    public void setActionSearchListeners() {
        curso_filtrado.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    iniciarBusqueda((AutoCompleteTextView) v);
                    return true;
                }
                return false;
            }
        });
    }

    public void ocultarTeclado(View view) {
        final InputMethodManager inputMethodManager = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        lupa.setVisible(false);
        cancelar.setVisible(false);
    }

    /**
     * Llama a la vista de crear reunión, enviando la ubicación de la nueva reunión
     *
     * @param latitude  La latitud de la nueva reunión
     * @param longitude la longitud de la nueva reunión
     * @param context   la referencia estática del contexto
     */

    public static void createReunion(double latitude, double longitude, Context context,
                                     Fragment parentFragment) {
    //    Toast.makeText(context, "Creando reunión en la ubicación: (" + latitude + "," + longitude + ")", Toast.LENGTH_LONG).show();
        Bundle parameter = new Bundle();
        parameter.putString("latitude", "" + latitude);
        parameter.putString("longitude", "" + longitude);
        Navigation.findNavController(parentFragment.getView()).navigate(R.id.action_nav_home_to_crearReunion, parameter);
    }


    public void confirmateReunionCreation(double latitude, double longitude) {

        //Despliega el mensaje de dialogo para confirmar la creación de la reunión
        ReunionConfirmationDialog dialog = new ReunionConfirmationDialog(latitude, longitude, getContext(),getParentFragment());
        dialog.show(getChildFragmentManager(), null);
    }


    /**
     * Despliega el snackBar para avisarle al usuario que puede crear la reunión, oculta todos los
     * <p>
     * marcadores, setea el listener del mapa para permitir crear la reunión y reconfigura el fab
     * <p>
     * para que cancele estos cambios una vez que se vuelva a presionar
     */

    public void allowCreateReunion() {
        //Despliega el snackview para indicarle al usuario que puede crear la reunión
        CoordinatorLayout layout = createdView.findViewById(R.id.mapFragmentLayout);
        creatingReunionSnackbar = Snackbar.make(layout, getResources().
                getText(R.string.creating_reunion), Snackbar.LENGTH_INDEFINITE);
        creatingReunionSnackbar.setBackgroundTint(getResources().getColor(R.color.colorPrimary));
        creatingReunionSnackbar.show();
        //Setea el listener del mapa para que al presionar por un tiempo prolongado se
        //solicite un cuadro de dialogo para confirmar la creación de la reunión
        map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            public void onMapLongClick(LatLng latLng) {
                confirmateReunionCreation(latLng.latitude, latLng.longitude);
            }
        });
        //Setea el listener del fab para que cancele la creación una vez que se vuelve a presionar
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelReunionCreation();
            }
        });
    }


    /**
     * Elimina el snackbar, elimina el listener del mapa para que no se pueda crear reuniones y
     * <p>
     * setea el listener del click del fab para que permita crear reuniones cuando se vuelva a
     * <p>
     * presionar
     */

    public void cancelReunionCreation() {
        if (creatingReunionSnackbar != null)
            creatingReunionSnackbar.dismiss();
        //Setea el listener del fab para que empiece el modo creación de renuión
        //una vez que se vuelve a presionar
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                allowCreateReunion();
            }
        });
        //Elimina el listener del mapa
        map.setOnMapLongClickListener(null);
    }


    /**
     * Despliega un mensaje de dialogo personalizado para confirmar la creación de la reunión
     */

    public static class ReunionConfirmationDialog extends DialogFragment {

        private double latitude, longitude;
        private static Context context;
        private Fragment parentFragment;
        public ReunionConfirmationDialog(double latitude, double longitude, Context context,
                                         Fragment parentFragment) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.context = context;
            this.parentFragment = parentFragment;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(
                    this.context, R.style.AlertDialogTheme));
            builder.setMessage("¿Desea crear la reunión en el sitio especificado?")
                    .setPositiveButton("    Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            createReunion(latitude, longitude, context, parentFragment);
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
    }
}

