package cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.repository;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.ActionCodeSettings;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthActionCodeException;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;

//import java.util.List;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.R;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.login.ViewModel.data.LoginResult;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.login.ViewModel.data.SentResult;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.dao.firebase.UsuarioDAO;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model.Usuario;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.Debug;

/**
 * How to make the perfect Singleton?
 * Keval Patel
 * https://medium.com/@kevalpatel2106/how-to-make-the-perfect-singleton-de6b951dfdb0
 * Muestra como crear una clase singleton que sea segura para varios hilos, reflexión y
 * serialización, no se toma en cuenta la reflexión y serialización ya que se estima que no serán
 * casos presentes en la aplicación
 * <p>
 * Se encarga de comunicar al view model con los dao y los webservices correspomdoemtes, en este
 * caso el de los Usuarios.
 *
 * @author Daniel Marín M
 */
public class UserRepository {

    private static volatile UserRepository userRepository;

    private MutableLiveData<SentResult> sentResultMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<LoginResult> loginResultMutableLiveData = new MutableLiveData<>();

//    private LiveData<List<Usuario>> usuariosLiveData;
    private LiveData<Usuario> usuarioLiveData;

    private FirebaseAuth auth;

    private UsuarioDAO usuarioDAO;

    private UserRepository() {
        auth = FirebaseAuth.getInstance();
        usuarioDAO = new UsuarioDAO();
//        usuariosLiveData = usuarioDAO.getUsuariosLiveData();
        usuarioLiveData = usuarioDAO.getUsuarioLiveData();

        Debug.log(auth.getCurrentUser());
        if (auth.getCurrentUser() != null) {
            loginResultMutableLiveData.setValue(new LoginResult(auth.getCurrentUser()));
        }
    }

    /**
     * Utiliza el patrón de bloqueo de doble verificación, para prevenir sincronización innecesaria
     * al detectar que el repositorio es nulo, esto previene que se creen instancias en paralelo.
     *
     * @return la única instancia del repositorio
     */
    public static UserRepository getInstance() {
        Debug.log();
        if (userRepository == null) {
            synchronized (UserRepository.class) {
                if (userRepository == null) {
                    userRepository = new UserRepository();
                }
            }
        }
        return userRepository;
    }

    /**
     * Implementa el envio de un correo con un enlace dinámico para iniciar sesión mediante Firebase
     * sin contraseña.
     * <p>
     * Cambia el resultado de envio de acuerdo a si el task es exitoso o no.
     *
     * @param packageName Utilizado para verificar si la última versuón de aplicación está
     *                    instalada en el dispositivo, si no es así se procede a abrir la
     *                    Play Store para que el usuario la instale, si no es posible
     *                    entonces se abre en enlace en la web a la página predefinida
     *                    (actualmente una página vacía).
     * @param correo      Correo electrónico a donde enviar el enlace dinámico.
     * @see SentResult
     */
    public void enviarEnlace(final String packageName, final String correo) {
        Debug.log();
        ActionCodeSettings ajustes = ActionCodeSettings.newBuilder()
                .setAndroidPackageName(packageName, false, null)
                .setHandleCodeInApp(true)
                .setUrl("https://estudiaderosucr.page.link/Ingresar")
                .build();
        auth.sendSignInLinkToEmail(correo, ajustes)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Debug.log("enviarEnlace");
                        sentResultMutableLiveData.setValue(new SentResult(correo));
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Debug.log("enviarEnlace", e.getMessage());
                        sentResultMutableLiveData.setValue(
                                new SentResult(R.string.error_sending_link));
                    }
                });
    }

    /**
     * Revisa si el enlace dado esá destinado a ser utilizado con
     * "ingresarPorEnlace(String correoInstitucional, String enlace)" el cual llama al método
     * "signInWithEmailLink(correo, enlace)" de Firebase.
     *
     * @param intentData datos del intent.
     * @return verdadero si tiene, falso si no.
     */
    public boolean esInicioMedianteEnlace(final String intentData) {
        boolean result = auth.isSignInWithEmailLink(intentData);
        Debug.log(result);
        return result;
    }

    /**
     * Implementa el ingreso sin contraseña de Firebase mediante un enlace.
     * <p>
     * Cambia el resultado de ingreso de acuerdo a si el task es exitoso o no.
     *
     * @param correo correo electrónico de donde proviene el enlace.
     * @param enlace obtenido como un enlace dinámico activado por el usuario.
     * @see LoginResult
     */
    public void ingresarPorEnlace(final String correo, final String enlace) {
        Debug.log();
        auth.signInWithEmailLink(correo, enlace)
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        Debug.log("ingresarPorEnlace");
                        loginResultMutableLiveData.setValue(new LoginResult(authResult.getUser()));
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Debug.log("ingresarPorEnlace", e.getMessage());
                        if (e instanceof FirebaseAuthActionCodeException) {
                            loginResultMutableLiveData.setValue(
                                    new LoginResult(R.string.error_invalid_link));
                        } else if (e instanceof FirebaseAuthInvalidCredentialsException) {
                            loginResultMutableLiveData.setValue(
                                    new LoginResult(R.string.error_invalid_email));
                        }
                    }
                });
    }

    /**
     * Implementa el cierre de sesión de Firebase, vacía el LoginResult.
     *
     * @see LoginResult
     */
    public void cerrarSesion(Usuario usuario) {
        Debug.log("antes",auth.getUid());
        if (usuario != null) {
            usuario.setSesion_iniciada(false);
            usuarioDAO.modificar(usuario);
        }
        auth.signOut();
        Debug.log("después",auth.getUid());
        loginResultMutableLiveData.setValue(new LoginResult(R.string.signed_out));
    }

    public void registrarCuenta(Usuario usuario) {
        FirebaseUser firebaseUser = auth.getCurrentUser();
        if (firebaseUser != null) {
            usuarioDAO.agregar(usuario);
        }
    }

    public void cargarUsuarioActual() {
        Debug.log(auth.getUid());
        if(auth.getUid() != null) {
            usuarioDAO.cargarDatosPorId(auth.getUid());
        }
    }

    public void modificarCuenta(@NonNull Usuario usuario) {
        Debug.log(usuario);
        if (usuario.getId().equals(auth.getUid())) {
            usuarioDAO.modificar(usuario);
        }
    }

    public void cancelarIngreso() {
        Debug.overlog();
        Debug.log("antes", auth.getUid());
        auth.signOut();
        loginResultMutableLiveData.setValue(
                new LoginResult(R.string.error_already_logged_in));
        Debug.log("después", auth.getUid());
    }

//    public void eliminarCuenta(Usuario usuario) {
//        Debug.log(usuario);
//        usuarioDAO.eliminar(usuario);
//    }

    public String getUid() {
        Debug.overlog(auth.getUid());
        return auth.getUid();
    }

    public LiveData<SentResult> getSentResult() {
        Debug.overlog(sentResultMutableLiveData);
        return sentResultMutableLiveData;
    }

    public LiveData<LoginResult> getLoginResult() {
        Debug.overlog(loginResultMutableLiveData);
        return loginResultMutableLiveData;
    }

//    public LiveData<List<Usuario>> getUsuariosLiveData() {
//        Debug.overlog(usuariosLiveData);
//        return usuariosLiveData;
//    }

    public LiveData<Usuario> getUsuarioLiveData() {
        Debug.overlog(usuarioLiveData);
        return usuarioLiveData;
    }
}