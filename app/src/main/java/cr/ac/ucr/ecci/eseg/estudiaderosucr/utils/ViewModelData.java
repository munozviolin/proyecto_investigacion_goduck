package cr.ac.ucr.ecci.eseg.estudiaderosucr.utils;

import androidx.annotation.Nullable;

/**
 * Define los métodos a utilizar para los objetos alojados como datos en tiempo real del
 * LoginViewModel
 *
 * @author Daniel Marín M
 */
public interface ViewModelData<T> {
    void setData(@Nullable T t);

    void setError(@Nullable Integer error);

    @Nullable
    T getData();

    @Nullable
    Integer getError();
}
