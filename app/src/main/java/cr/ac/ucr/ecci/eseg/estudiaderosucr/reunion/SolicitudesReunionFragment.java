package cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;

import java.util.LinkedList;
import java.util.List;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.R;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.model.Reunion;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.Listas.Adapters.SolicitudesPageAdapter;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.Listas.Fragmentos.SolicitudesPendientesFragment;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.Listas.Fragmentos.SolicitudesProcesadasFragment;



/**
 * Este fragmento se encarga de desplegar una lista con todas las solicitudes pendientes de
 * una reunión en específico, la cual se determina según el ID de reunión recibido.
 *
 * Consiste en una lista que despliega cada solicitud, con la opción de aprobar o rechazar dicha
 * solicitud. Cada acción muestra un cuadro de confirmación
 */

public class SolicitudesReunionFragment extends Fragment  {


    private String idReunion = "";

    public SolicitudesReunionFragment() {

    }



    public static SolicitudesReunionFragment newInstance(String param1, String param2) {
        SolicitudesReunionFragment fragment = new SolicitudesReunionFragment();

        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_solicitudes_reunion, container,
                false);

        //Obtiene los componentes requeridos
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.solicitudesTabLayout);
        final ViewPager viewPager = (ViewPager) view.findViewById(R.id.solicitudesViewPager);

        tabLayout.setupWithViewPager(viewPager);

        //Recibe el ID de la reunión por parámetro
        Bundle bundle = this.getArguments();

        //Crea las listas requeridas por el adaptador de páginas
        List<Fragment> listaPantallas = new LinkedList<>();
        List<String> listaTitulosPantallas = new LinkedList<>();


        Fragment solicitudesPendientesFragment = null;
        Fragment solicitudesProcesadasFragment = null;

        if (bundle != null){
            idReunion = bundle.getString("ID","");
        }

        listaPantallas.add(solicitudesPendientesFragment);
        listaPantallas.add(solicitudesProcesadasFragment);
        listaTitulosPantallas.add(getResources().getString(R.string.solicitudes_pendientes_tab));
        listaTitulosPantallas.add(getResources().getString(R.string.solicitudes_revisadas_tab));

        //Crea el nuevo adaptador y setea los fragmentos correspondientes
        final SolicitudesPageAdapter solicitudesPageAdapter = new SolicitudesPageAdapter(
                getChildFragmentManager(),listaPantallas,listaTitulosPantallas, idReunion);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

               solicitudesPageAdapter.notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewPager.setAdapter(solicitudesPageAdapter);



        return view;
    }
}
