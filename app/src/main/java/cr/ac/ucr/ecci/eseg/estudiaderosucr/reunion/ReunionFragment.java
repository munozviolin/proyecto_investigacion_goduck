package cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.Button;
import android.widget.TextView;
import android.content.Intent;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.R;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.comentario.adapter.CommentAdapter;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.comentario.dao.firebase.ComentarioDAO;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.comentario.model.Comentario;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.dao.firebase.ReunionesDAO;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.model.Reunion;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.dao.firebase.SolicitudDAO;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.solicitud.model.Solicitud;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.repository.UserRepository;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReunionFragment extends Fragment implements View.OnClickListener, CommentAdapter.AdapterCallback {


    //Variables dedicadas al View/Layout
    private static final int GALLERY_REQUEST = 77;
    private Uri filepath;
    private Bitmap bitmap;
    private ImageView imagenComentario;
    private ImageView imagenLista;
    private Button buttonCargarImagen;
    private Button buttonAbrirEnMapas;
    private Button buttonGuardarEnCalendario;
    private Button buttonAgregarComentario;
    private Button buttonAgregarRespuestaComentario;
    private TextView confirmacionImagen;
    private TextView textReunionName;
    private TextView textPorUsuario;
    private TextView textFechaHora;
    private EditText textComentario;

    private TextView textDescripcion;
    private TextView textMateria;

    private ImageButton editButton;

    private String idDeReunion;
    private String pathImagen;

    private Reunion reunionActual;

    private List<Reunion> reuniones;
    private ReunionesDAO reunion;
    private View v;
    private double latitud;
    private double longitud;
    private String idUsuarioAutenticado;
    private ProgressBar progressBar;
    private RecyclerView commentList;
    private CommentAdapter adapter;
    //Variables dedicadas a la pertinencia de datos temporales
    private List<Comentario>listaComentariosEnReunion;
    //Variables de interaccion con Firebase
    private ComentarioDAO comentarioDAO;
    private SolicitudDAO solicitudDAO;

    private String nombreReunion;
    private String dia;
    private String mes;
    private String anio;
    private String hora;
    private String minuto;

    private String idSolicitud;
    private boolean isRespuesta;
    private int posicionItem;


    public ReunionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        reunion = new ReunionesDAO();
        comentarioDAO = new ComentarioDAO();
        solicitudDAO = new SolicitudDAO();
        this.isRespuesta = false;

        final Comentario comentario = new Comentario();

        reunion.cargarDatos();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            idDeReunion = bundle.getString("ID", "");
            comentarioDAO.cargarDatosPorId(idDeReunion);
            //solicitudDAO.cargarDatoPorId(idDeReunion);
            solicitudDAO.cargarSolicitudPorReunionUsuario(UserRepository.getInstance().getUid()
                    ,idDeReunion);
        }

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                    getReunionByID();
            }
        }, 500);
        idUsuarioAutenticado = UserRepository.getInstance().getUid();

        v = inflater.inflate(R.layout.fragment_reunion, container, false);
        textReunionName = (TextView) v.findViewById(R.id.TextReunionName);
        textFechaHora = (TextView) v.findViewById(R.id.TextFechaHora);
        textDescripcion = v.findViewById(R.id.textDescripcion);
        textMateria = v.findViewById(R.id.textMateria);
        buttonAbrirEnMapas = (Button) v.findViewById(R.id.buttonAbrirEnMapas);
        commentList = (RecyclerView) v.findViewById(R.id.comment_section);
        textComentario = (EditText) v.findViewById(R.id.comment_input);
        buttonCargarImagen = (Button) v.findViewById(R.id.upload_picture);
        confirmacionImagen = (TextView) v.findViewById(R.id.picture_ready);
        imagenLista = (ImageView) v.findViewById(R.id.check);
        buttonAgregarRespuestaComentario = (Button) v.findViewById(R.id.boton_responder);
        progressBar = (ProgressBar) v.findViewById(R.id.loading_comments);
        editButton = v.findViewById(R.id.editButton);


        commentList.setVisibility(View.INVISIBLE);
        textComentario.setHint(R.string.escribe_comentario);

        buttonAbrirEnMapas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                irMapa();
            }
        });

        //boton para la carga de una imagen de la galeria del sistema
        buttonCargarImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
            }
        });

        buttonGuardarEnCalendario = (Button) v.findViewById(R.id.buttonGuardarEnCalendario);
        buttonGuardarEnCalendario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agregarEventoACalendario();
            }
        });

        //boton para anadir comentario
        buttonAgregarComentario = (Button) v.findViewById(R.id.send_comment);
        //accion del boton enviar comentario
        buttonAgregarComentario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((textComentario.getText().toString().replace(" ","").length()> 0  || filepath != null) && textComentario.getText() != null) {

                    final String date = new SimpleDateFormat("dd/MM", Locale.getDefault()).format(new Date()); //obtiene la fecha y hora actual del dispositivo
                    if (!isRespuesta) {

                        comentario.setId(UUID.randomUUID().toString()); //asigna el id al comentario
                        comentario.setIdReunion(idDeReunion); //asigna el id de la reunion al modelo
                        comentario.setDescripcion(textComentario.getText().toString()); //asigna la descripcion del comentario al modelo
                        comentario.setFechaComentario(date); //asigna fecha al comentario

                        //si el comentario tiene una ruta de imagen la carga
                        if (filepath != null) {
                            cargarImagen(comentario); //carga la imagen
                            comentario.setTieneImagen(true); //el comentario tiene imagen
                        }
                        comentarioDAO.agregar(comentario); //agregar la tupla
                        commentList.getAdapter().notifyItemChanged(posicionItem); //actualiza el adaptador del RecyclerView de comentarios
                    } else {
                        Comentario respuesta = new Comentario(UUID.randomUUID().toString(), textComentario.getText().toString(),
                                date, idDeReunion, idUsuarioAutenticado);

                        if (listaComentariosEnReunion.get(posicionItem).getListaRespuestas() == null)
                            listaComentariosEnReunion.get(posicionItem).setListaRespuestas(new ArrayList<Comentario>());

                        listaComentariosEnReunion.get(posicionItem).getListaRespuestas().add(respuesta);
                        listaComentariosEnReunion.get(posicionItem).setTieneRespuesta(true);

                        //si la respuesta tiene una ruta de imagen la carga
                        if (filepath != null) {
                            cargarImagen(respuesta); //carga la imagen
                            respuesta.setTieneImagen(true); //la respuesta tiene imagen
                        }

                        comentarioDAO.modificar(listaComentariosEnReunion.get(posicionItem));
                        commentList.getAdapter().notifyItemChanged(posicionItem);

                        isRespuesta = false;
                        textComentario.setHint(R.string.escribe_comentario);


                    }
                    textComentario.getText().clear();
                }else{
                    Toast.makeText(getContext(), "Mensaje vacío", Toast.LENGTH_SHORT);
                }
            }
        });

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mostrarComentarios();
                progressBar.setVisibility(View.GONE);
                commentList.setVisibility(View.VISIBLE);

            }
        }, 2000);


        return v;
    }

    private void agregarEventoACalendario() {
        Calendar calendar = Calendar.getInstance();
        Intent intent = null;

        try {
            calendar.set(Calendar.YEAR, Integer.parseInt(anio));
            calendar.set(Calendar.MONTH, Integer.parseInt(mes));
            calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dia));
            calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hora));
            calendar.set(Calendar.MINUTE, Integer.parseInt(minuto));

            intent = new Intent(Intent.ACTION_INSERT);//O EDIT
            intent.setType("vnd.android.cursor.item/event");
            intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, calendar.getTimeInMillis());
            intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, calendar.getTimeInMillis() + 60 * 60 * 1000);

            intent.putExtra(CalendarContract.Events.TITLE, nombreReunion);
            startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(requireContext(), "Hubo un error, no se pudo abrir la aplicación de calendario", Toast.LENGTH_LONG).show();
        }

    }


    private void asignarDatosParaCalendario(String fechaP) {
        String fecha = fechaP.split(" ")[0];
        String horaCompleta = fechaP.split(" ")[1];
        String meridiano = fechaP.split(" ")[2];

        dia = fecha.split("/")[0];
        mes = Integer.parseInt(fecha.split("/")[1]) - 1 + "";
        anio = fecha.split("/")[2];

        int horaEntera = Integer.parseInt(horaCompleta.split(":")[0]);
        minuto = horaCompleta.split(":")[1];

        if (meridiano.equals("pm")) {
            horaEntera += 12;
        }
        hora = "" + horaEntera;
    }

    //Indica si el usuario es el creador o no de la reunión
    public boolean usuarioEsCreadorReunion(String idUsuario) {
        //todo solucionar esto cuando ya no falte user auth
        return idUsuario.equals(idUsuarioAutenticado);

        // return (true == idUsuario.equals("ignacio"));
    }

    /**
     * Abre la aplicación de Maps con la ubicación de la reunión
     */
    private void irMapa() {
        // Intent para ver la localización en el mapa
        String url = "geo:" + latitud + "," +longitud;
        String q = "?q="+ latitud + "," +
                longitud + "(" + "repaso" + ")";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url + q));
        intent.setPackage("com.google.android.apps.maps");
        startActivity(intent);
    }

    public String obtenerEstadoSolicitud(String idUsuario, String idReunion) {
        String estadoSolicitud = Solicitud.SIN_SOLICITUD;

        //Obtiene la solicitud del Dao de Solicitudes
        Solicitud solicitudActual = solicitudDAO.getSolicitudCargada();

        if(solicitudActual != null) {
            idSolicitud = solicitudActual.getId();
            estadoSolicitud = solicitudActual.getEstado();
        }

        //Si obtiene nulo, la solicitud es nula y devuelve SIN_SOLICITUD
        //Si obtiene la solicitud, devuelve el estado de la solicitud
        return estadoSolicitud;
    }


    public void habilitarAgregarSolicitud(final Button buttonSolicitudes) {
        buttonSolicitudes.setText("Solicitar Unión");
        buttonSolicitudes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Inserta la solicitud en la base
                Solicitud nuevaSolicitud = new Solicitud();
                nuevaSolicitud.setIdUsuario(UserRepository.getInstance().getUid());
                nuevaSolicitud.setIdReunion(idDeReunion);
                nuevaSolicitud.setEstado(Solicitud.SOLICITUD_PENDIENTE);

                int random = ThreadLocalRandom.current().nextInt(0, 999 + 1); //genera un id
                nuevaSolicitud.setId(String.valueOf(random)); //asigna el id al comentario

                solicitudDAO.agregar(nuevaSolicitud);

                habilitarCancelarSolicitud(buttonSolicitudes);
            }
        });
    }

    public void habilitarCancelarSolicitud(final Button buttonSolicitudes) {
        buttonSolicitudes.setText("Cancelar Solicitud");
        buttonSolicitudes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Solicitud solicitud = new Solicitud();
                solicitud.setIdUsuario(UserRepository.getInstance().getUid());
                solicitud.setIdReunion(idDeReunion);

                //Actualiza el id de la reunión para permitir eliminar de una la solicitud actual
                obtenerEstadoSolicitud(solicitud.getIdUsuario(),solicitud.getIdReunion());

                solicitud.setId(idSolicitud);

                //Elimina la solicitud
                solicitudDAO.eliminar(solicitud);

                habilitarAgregarSolicitud(buttonSolicitudes);
            }
        });
    }

    /**
     * Modifica el texto y el comportamiento del boton de solicitudes dependiendo de si el usuario
     * autenticado es el autor de la reuniòn, o si tiene una solicitud, del estado de dicha solicitud.
     * @param reunionActual Instancia de la reunion
     */
    public void configurarBotonSolicitudes(Reunion reunionActual) {
        final Button buttonSolicitudes = (Button) v.findViewById(R.id.buttonSolicitudes);

        //Verifica si el usuario actual es el creador de la reunion
        if (usuarioEsCreadorReunion(reunionActual.getIdUsuario())) {
            //Si es el creador, mantiene el texto del botón, pero setea un listener
            //para llevar al usuario a la vista de  revisar solicitudes

            buttonSolicitudes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Redirige al fragmento de la lista de solicitudes
                    Bundle parameter = new Bundle();
                    parameter.putString("ID",idDeReunion);
                    Navigation.findNavController(getParentFragment().getView()).
                            navigate(R.id.action_reunionFragment_to_solicitudesReunionFragment,
                                    parameter);
                }
            });

        } else {
            //Revisa el estado de la solicitud del usuario
            String estadoSolicitud = obtenerEstadoSolicitud(reunionActual.getId(),
                    reunionActual.getIdUsuario()).toUpperCase();

            switch(estadoSolicitud)
            {
                case Solicitud.SIN_SOLICITUD:
                    habilitarAgregarSolicitud(buttonSolicitudes);

                    break;
                case Solicitud.SOLICITUD_PENDIENTE:
                    habilitarCancelarSolicitud(buttonSolicitudes);
                    break;
                case Solicitud.SOLICITUD_ACEPTADA:
                    buttonSolicitudes.setOnClickListener(null);
                    buttonSolicitudes.setText("Solicitud Aceptada");
                    break;
                case Solicitud.SOLICITUD_RECHAZADA:
                    buttonSolicitudes.setOnClickListener(null);
                    buttonSolicitudes.setText("Solicitud Rechazada");
                    break;

            }
        }
    }

    /**
     * Encuentra la reunión seleccionada en el mapa y carga los datos
     * de la reunión en el fragment de reunión
     */
    private void getReunionByID() {
        reuniones = reunion.getReuniones();
        if (reuniones != null) {
            for (Reunion r : reuniones) {
                if (r.getId().equals(idDeReunion)) {
                    reunionActual = r;
                    cambiarNombreReunion(r.getTitulo());
                    latitud = r.getLatitud();
                    longitud = r.getLongitud();

                    nombreReunion = r.getTitulo();
                    asignarDatosParaCalendario(r.getHoraInicio());

                    cambiarMateria(r.getCurso());
                    cambiarDescripcion(r.getDescripcion());
                    cambiarFechaYHora(r.getHoraInicio());

                    configurarBotonSolicitudes(r);

                    editButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle parameter = new Bundle();
                            parameter.putParcelable("reunion", reunionActual);
                            Navigation.findNavController(getParentFragment().getView()).navigate(R.id.action_reunionFragment_to_editarReunion, parameter);
                        }
                    });
                }
            }
        } else {
            Toast.makeText(requireContext(), "Hubo un error, no se encontró la reunión", Toast.LENGTH_LONG).show();
        }

    }

    private void cambiarMateria(String curso) {
        textMateria.setText(curso);
    }
    private void cambiarDescripcion(String descripcion) {
        textDescripcion.setText(descripcion);
    }

    private void mostrarComentarios() {


        this.listaComentariosEnReunion =  comentarioDAO.getComentariosPorReunion();
        if(listaComentariosEnReunion != null) {
            //ordenar
            adapter = new CommentAdapter(getContext(), listaComentariosEnReunion, this);
            commentList.setAdapter(adapter);
            commentList.setLayoutManager(new LinearLayoutManager(this.getContext()));
        }else{

            Toast.makeText(requireContext(), "No hay comentarios :(", Toast.LENGTH_LONG).show();
        }

    }

//    /**
//     * Carga el nombre del usuario en el textview de usuario
//     *
//     * @param autor el nombre del autor de la reunión
//     */
//    public void cambiarTextoUsuario(String autor) {
//        textPorUsuario.setText(textPorUsuario.getText().toString() + " " + autor);
//    }

    /**
     * Carga el nombre de la reunión en el textview de reunión
     *
     * @param nombre el nombre del autor de la reunión
     */
    public void cambiarNombreReunion(String nombre) {
        textReunionName.setText(nombre);
    }

    /**
     * Carga la fecha y hora de la reunión en el textview de fecha y hora
     *
     * @param fecha el nombre del autor de la reunión
     */
    public void cambiarFechaYHora(String fecha) {
        textFechaHora.setText(fecha);
    }

    @Override
    public void onClick(View view) {

    }

    //funcion que es llamada para cargar imagen de la galeria del sistema
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK)
            switch (requestCode){
                case GALLERY_REQUEST:
                    filepath = data.getData(); //ruta de la imagen escogida por el usuario
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filepath); //bitmap que albergara la imagen
                        //se obtiene el nombre de la imagen en el sistema para mostrarle al usuario el archivo que carga
                        Cursor returnCursor = getContext().getContentResolver().query(filepath, null, null, null, null);
                        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                        returnCursor.moveToFirst();
                        confirmacionImagen.setText("Publicar: " + returnCursor.getString(nameIndex)); //mensaje que muestra al usuario cual imagen cargo

                        //se muestra el mensaje al usuario y un icono
                        imagenLista.setVisibility(View.VISIBLE);
                        confirmacionImagen.setVisibility(View.VISIBLE);
                    } catch (IOException e) {
                        Log.i("TAG", "Error al cargar la imagen, vuelva a intentarlo " + e);
                    }
                    break;
            }
    }

    //carga la imagen del comentario al storage de Firebase
    private void cargarImagen(Comentario comentario) {
        pathImagen = "images/"+ UUID.randomUUID().toString(); //se asigna un id a la imagen cargada por el usuario
        comentario.setImagenComentario(pathImagen); //almacena la ruta de la imagen en la base de datos
        FirebaseStorage storage = FirebaseStorage.getInstance(); //instancia de Firebase Storage
        StorageReference storageReference = storage.getReference(); //referencia de la instancia
        StorageReference ref = storageReference.child(pathImagen); //crea una referencia hija de la referencia de la instancia
        ref.putFile(filepath); //sube la imagen al storage de Firebase

        //una vez que se envio el comentario de imagen desaparece el mensaje e icono de confirmacion y se limpian las variables
        imagenLista.setVisibility(View.INVISIBLE);
        confirmacionImagen.setVisibility(View.INVISIBLE);
        bitmap = null;
        pathImagen = null;
        filepath = null;
        comentario.setTieneImagen(false);
    }

    @Override
    public void onMethodCallBack(int position) {
        this.isRespuesta = true;
        this.posicionItem = position;
        textComentario.setHint(R.string.escribe_respuesta);
        ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
                .showSoftInput(textComentario, InputMethodManager.SHOW_FORCED);
    }
}