package cr.ac.ucr.ecci.eseg.estudiaderosucr.login;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.R;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.login.ViewModel.LoginViewModel;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.login.ViewModel.LoginViewModelFactory;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.login.ViewModel.data.FormState;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.login.ViewModel.data.LoginResult;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.login.ViewModel.data.SentResult;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.main.MainActivity;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model.Usuario;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.Debug;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.TextChangedListener;

import static cr.ac.ucr.ecci.eseg.estudiaderosucr.login.LoginActivity.CORREO_INSTITUCIONAL;
import static cr.ac.ucr.ecci.eseg.estudiaderosucr.login.LoginActivity.REGISTRO;
import static cr.ac.ucr.ecci.eseg.estudiaderosucr.login.LoginActivity.USUARIO;
import static cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.UIUtils.ocultarTeclado;

public class LoginFragment extends Fragment {
    public static final String SALIR = "salir";

    private LoginViewModel viewModel;

    private CredentialsSharedPreferences preferences;


    private ConstraintLayout constraintLayout;
    private TextView tvEstado;
    private EditText etCorreoInstitucional;
    private CheckBox cbRecordarCredenciales;
    private ProgressBar progressBar;
    private Button btnEnviarEnlace;
    private Button btnAbrirCorreoInstitucional;
    private Button btnAbrirGmail;


    static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Debug.log();

        View view = inflater.inflate(R.layout.fragment_login, container, false);
        tvEstado = view.findViewById(R.id.tv_frg_login_status);
        etCorreoInstitucional = view.findViewById(R.id.et_frg_login_email);
        cbRecordarCredenciales = view.findViewById(R.id.cb_frg_login_remember);
        progressBar = view.findViewById(R.id.pg_frg_login);
        btnEnviarEnlace = view.findViewById(R.id.btn_frg_login_send);
        btnAbrirCorreoInstitucional = view.findViewById(R.id.btn_frg_login_go_to_ucr);
        btnAbrirGmail = view.findViewById(R.id.btn_frg_login_open_gmail);
        constraintLayout = view.findViewById(R.id.sv_frg_login_main);

        // listeners
        setOnClickListeners();
        addTextChangedListener();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Debug.log();

        LoginViewModelFactory factory = new LoginViewModelFactory();
        ViewModelProvider provider = new ViewModelProvider(requireActivity(), factory);
        viewModel = provider.get(LoginViewModel.class);

        if(viewModel.getUid() == null){
            constraintLayout.setVisibility(View.VISIBLE);
        }

        String correo = viewModel.getCorreo();
        etCorreoInstitucional.setText(correo);
        cbRecordarCredenciales.setChecked(correo != null);

        preferences = new CredentialsSharedPreferences(requireContext());

        //observers
        setObservers();
        revisarIntent();
    }


    @Override
    public void onResume() {
        super.onResume();
        Debug.log();
        etCorreoInstitucional.setText(viewModel.getEtCorreoInstitucionalValue());
        cbRecordarCredenciales.setChecked(viewModel.getCbRecordarCredencialesValue());
    }

    private void setOnClickListeners() {
        Debug.log();
        btnEnviarEnlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enviarEnlace();
            }
        });
        btnAbrirCorreoInstitucional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirCorreoInstitucional();
            }
        });
        btnAbrirGmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirGmail();
            }
        });
    }

    private void addTextChangedListener(){
        etCorreoInstitucional.addTextChangedListener(new TextChangedListener(){
            @Override
            public void afterTextChanged(Editable s) {
                boolean recordar = cbRecordarCredenciales.isChecked();
                if(!s.toString().isEmpty()) {
                    viewModel.validarFormulario(s.toString(), recordar);
                }
            }
        });
    }

    private void setObservers() {
        Debug.log();

        // Habilita o deshabilita los botones dependiendo del estado del formulario.
        viewModel.getFormState().observe(getViewLifecycleOwner(),
                new Observer<FormState>() {
                    @Override
                    public void onChanged(@Nullable FormState formState) {
                        Debug.log("loginFormState:", formState);
                        if (formState != null) {
                            btnEnviarEnlace.setEnabled(formState.esValido());
                            Integer error = formState.getError();
                            if (error != null) {
                                etCorreoInstitucional.setError(getString(R.string.et_inst_email_error));
                            }else{
                                etCorreoInstitucional.setError(null);
                            }
                        }
                    }
                });

        // Cambia la interfaz en base al resultado del envío.
        viewModel.getSentResult().observe(requireActivity(), new Observer<SentResult>() {
            @Override
            public void onChanged(SentResult sentResult) {
                Debug.log("SentResult:", sentResult);
                if (sentResult != null) {
                    String correo = sentResult.getData();
                    Integer error = sentResult.getError();
                    if (correo != null) {
                        setUpIngreso();
                    } else if (error != null) {
                        setUpError(error);
                    }
                }
            }
        });

        // Cambia la interfaz en base al resultado del ingreso.
        viewModel.getLoginResult().observe(requireActivity(), new Observer<LoginResult>() {
            @Override
            public void onChanged(@Nullable LoginResult loginResult) {
                Debug.log("LoginResult:", loginResult);
                if (loginResult != null) {
                    progressBar.setVisibility(View.INVISIBLE);
                    Integer error = loginResult.getError();
                    if (loginResult.getData() != null) {
                        viewModel.cargarUsuarioActual();
                    } else if (error != null) {
                        setUpError(error);
                    }
                }
            }
        });

        viewModel.getUsuario().observe(requireActivity(), new Observer<Usuario>() {
            @Override
            public void onChanged(Usuario usuario) {
                if (!requireActivity().getIntent().getBooleanExtra(SALIR, false)) {
                    viewModel.getUsuario().removeObservers(requireActivity());
                    Debug.log("Usuario:", usuario);

                    if (usuario != null) {
                        String uuid = viewModel.getUuid();
                        if (!usuario.isSesion_iniciada() || usuario.getUuid().equals(uuid)) {
                            viewModel.modificarUsuario(usuario);
                            Activity activity = requireActivity();
                            Intent intent = new Intent(requireContext(), MainActivity.class);
                            intent.putExtra(USUARIO,usuario);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            activity.setResult(Activity.RESULT_OK);
                            activity.finish();
                        } else {
                            viewModel.cancelarIngreso();
                        }
                    } else {
                        if (preferences.cargarPreferencia(REGISTRO) == null) {
                            preferences.guardarPreferencia(REGISTRO, "registrando");
                            setUpRegistro();
                        }
                    }
                }
            }
        });
    }


    private void abrirGmail() {
        Debug.log();
        preferences.persistirPreferencias(cbRecordarCredenciales.isChecked());
        PackageManager manager = requireActivity().getPackageManager();
        Intent intent = manager.getLaunchIntentForPackage("com.google.android.gm");
        startActivity(intent);
    }

    // TODO: utilizar un webview con el html manipulado de forma que sea responsivo
    private void abrirCorreoInstitucional() {
        Debug.log("https://correo.ucr.ac.cr");
        preferences.persistirPreferencias(cbRecordarCredenciales.isChecked());
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://correo.ucr.ac.cr"));
        startActivity(intent);
    }


    private void revisarIntent() {
        Intent intent = requireActivity().getIntent();
        Debug.log(intent);
        if (viewModel.esInicioMedianteEnlace(intent.getDataString())) {
            Debug.log("ingreso");
            ingresar();
        } else if (intent.getBooleanExtra(SALIR, false)) {
            Debug.log("reinicio");
            setUpCierre();
        } else {
            Debug.log("set up enviar enlace");
            setupEnvio();
        }
    }

    private void setupEnvio() {
        Debug.log(requireContext().getString(R.string.status_start));
        reiniciarUI();
        btnEnviarEnlace.setText(R.string.action_send_link);
        tvEstado.setText(R.string.status_start);
    }

    private void enviarEnlace() {
        String correo = etCorreoInstitucional.getText().toString();
        Debug.log(correo);
        progressBar.setVisibility(View.VISIBLE);
        ocultarTeclado(requireContext(), etCorreoInstitucional);
        viewModel.enviarEnlace(requireActivity().getPackageName(), correo);
        preferences.guardarPreferencia(CORREO_INSTITUCIONAL, correo);
        preferences.persistirPreferencias(cbRecordarCredenciales.isChecked());
    }

    private void setUpIngreso() {
        btnEnviarEnlace.setText(R.string.action_resend_link);
        btnAbrirCorreoInstitucional.setVisibility(View.VISIBLE);
        btnAbrirGmail.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
        String correo = etCorreoInstitucional.getText().toString();
        String estado = requireContext().getString(R.string.status_email_sent) + " " + correo;
        Debug.log(estado);
        tvEstado.setText(estado);
    }

    private void ingresar() {
        String correo = preferences.cargarPreferencia(CORREO_INSTITUCIONAL);
        viewModel.validarFormulario(correo,cbRecordarCredenciales.isChecked());
        if(viewModel.getEtCorreoInstitucionalValue() != null) {
            Debug.log(correo);
            preferences.BorrarPreferencia(CORREO_INSTITUCIONAL);
            progressBar.setVisibility(View.VISIBLE);
            viewModel.ingresarPorEnlace(correo);
        }else{
            setUpError(R.string.error_invalid_link);
        }
    }

    private void setUpRegistro() {
        RegistroFragment registroUsuarioFragment = RegistroFragment.newInstance();

        FragmentManager manager = requireActivity().getSupportFragmentManager();

        ConstraintLayout constraintLayout = requireActivity().findViewById(R.id.cl_act_login);
        TransitionManager.beginDelayedTransition(constraintLayout);

        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.login_fragment_container, registroUsuarioFragment);
        transaction.commit();
    }

    private void setUpError(int error) {
        Debug.log();
        tvEstado.setText(getText(error));
        btnEnviarEnlace.setText(R.string.action_resend_link);
        btnAbrirCorreoInstitucional.setVisibility(View.VISIBLE);
        btnAbrirGmail.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void setUpCierre() {
        Debug.log();
        reiniciarUI();
        String status = requireContext().getString(R.string.status_logout) + " " +
                requireContext().getString(R.string.status_start);
        tvEstado.setText(status);
    }

    private void reiniciarUI() {
        Debug.log();
        btnEnviarEnlace.setText(R.string.action_send_link);
        btnAbrirCorreoInstitucional.setVisibility(View.GONE);
        btnAbrirGmail.setVisibility(View.GONE);
        progressBar.setVisibility(View.INVISIBLE);
    }
}