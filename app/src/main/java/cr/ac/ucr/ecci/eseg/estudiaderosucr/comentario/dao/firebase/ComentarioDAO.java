package cr.ac.ucr.ecci.eseg.estudiaderosucr.comentario.dao.firebase;

import androidx.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.comentario.model.Comentario;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.FireBaseHelper;

public class ComentarioDAO implements FireBaseHelper<Comentario> {

    private static final String COMENTARIOS = "comentarios";
    private List<Comentario> listaComentarios;
    private List<Comentario>comentariosPorReunion;

    public ComentarioDAO() {
    }

    //agregar un comentario a la base de datos
    @Override
    public void agregar(Comentario objeto) {

        //se obtiene el usuario actual de la aplicacion
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        //TODO: reparar con respecto al usuario actual
        String userId = firebaseUser.getUid(); //id del usuario actual
        objeto.setIdUsuario(userId); //anade el usuario como autor del comentario
        DatabaseReference mRef =  database.getReference(COMENTARIOS); //referencia de la tabla comentarios
        mRef.child(objeto.getId()).setValue(objeto); //inserta la nueva tupla
    }

    //eliminar un comentario de la base de datos
    @Override
    public void eliminar(Comentario comentario) {
        DatabaseReference mRef =  database.getReference(COMENTARIOS); //referencia de la tabla comentarios
        mRef.child(comentario.getId()).removeValue(); //elimina la tupla con el id de comentario respectivo
    }

    @Override
    public void cargarDatos() {

        final List<Comentario> list = new ArrayList<>();
        DatabaseReference reference = database.getReference(COMENTARIOS);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot data: dataSnapshot.getChildren()) {

                    list.add(data.getValue(Comentario.class));

                }
                setListaComentarios(list);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void cargarDatosPorId(String id) {

        final List<Comentario> list = new ArrayList<>();
        DatabaseReference reference = database.getReference(COMENTARIOS);
        reference.orderByChild("idReunion").equalTo(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot data: dataSnapshot.getChildren()) {

                    list.add(data.getValue(Comentario.class));

                }
                setComentariosPorReunion(list);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void modificar(Comentario objetoNuevo) {

        DatabaseReference reference = database.getReference(COMENTARIOS);
        reference = reference.child(objetoNuevo.getId());
        reference.setValue(objetoNuevo);

    }

    public List<Comentario> getListaComentarios() {
        return listaComentarios;
    }

    public void setListaComentarios(List<Comentario> listaComentarios) {
        this.listaComentarios = listaComentarios;
    }

    public List<Comentario> getComentariosPorReunion() {
        return comentariosPorReunion;
    }

    public void setComentariosPorReunion(List<Comentario> comentariosPorReunion) {
        this.comentariosPorReunion = comentariosPorReunion;
    }
}