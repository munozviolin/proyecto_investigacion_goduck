package cr.ac.ucr.ecci.eseg.estudiaderosucr.login;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKey;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.Debug;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.SharedPreferencesHelper;

/**
 * Maneja y encrypta las preferencias compartidas sobre credenciales
 *
 * @author Daniel Marín M
 */
public class CredentialsSharedPreferences implements SharedPreferencesHelper<String> {

    private static final String CREDENCIALES = "credenciales";

    private SharedPreferences sharedPreferences;
    private String pathname;

    CredentialsSharedPreferences(Context context) {
        Debug.overlog();
        crearPreferencias(context);
    }

    @Override
    public void crearPreferencias(Context context) {
        try {
            MasterKey.Builder builder = new MasterKey.Builder(context);
            builder.setKeyScheme(MasterKey.KeyScheme.AES256_GCM);
            MasterKey masterKey = builder.build();
            sharedPreferences = EncryptedSharedPreferences.create(
                    context,
                    CREDENCIALES,
                    masterKey,
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            );
            String appAbsolutePath = context.getFilesDir().getPath();
            pathname = appAbsolutePath + "/shared_prefs/" + CREDENCIALES + ".xml";
        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }
        Debug.overlog("Preferencias creadas:", pathname);
    }

    @Override
    public void guardarPreferencia(String llave, String valor) {
        Debug.overlog("Guardando {" + llave + ":" + valor + "}.");
        sharedPreferences.edit().putString(llave, valor).apply();
    }

    @Override
    public String cargarPreferencia(String llave) {
        Debug.overlog("Cargando ", llave);
        return sharedPreferences.getString(llave, null);
    }

    @Override
    public void BorrarPreferencia(String llave) {
        if (!sharedPreferences.getBoolean(PERSISTIR, false)) {
            Debug.overlog("Borrando ", llave);
            sharedPreferences.edit().remove(llave).apply();
        }
    }

    @Override
    public void eliminarPreferencias() {
        if (!sharedPreferences.getBoolean(PERSISTIR, false)) {
            sharedPreferences.edit().clear().apply();
            File file = new File(pathname);
            file.deleteOnExit();
            Debug.overlog("Eliminando", pathname);
        }
    }

    @Override
    public void persistirPreferencias(Boolean persistir) {
        Debug.overlog("Persistencia activada");
        sharedPreferences.edit().putBoolean(PERSISTIR, persistir).apply();
    }
}

