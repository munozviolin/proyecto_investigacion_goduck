package cr.ac.ucr.ecci.eseg.estudiaderosucr.database.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.comentario.model.Comentario;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.comentario.dao.room.ComentarioDAO;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.model.Reunion;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.dao.room.ReunionDAO;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model.Usuario;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.dao.room.UsuarioDAO;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.database.room.dao.UsuarioReunionJoinDAO;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.database.room.model.UsuariosReunionJoin;

@Database(entities = {Comentario.class, Reunion.class, Usuario.class, UsuariosReunionJoin.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static final String DB_NAME = "appDatabase.db";
    private static volatile AppDatabase instance;

    //simple patrón singleton para evitar acceso a múltiples instancias
    public static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }

    //crea la base de datos
    private static AppDatabase create(final Context context) {
        return Room.databaseBuilder(
                context,
                AppDatabase.class,
                DB_NAME).build();
    }

    public abstract ComentarioDAO getComentarioDAO();
    public abstract UsuarioDAO getUsuarioDAO();
    public abstract ReunionDAO getReunionDAO();
    public abstract UsuarioReunionJoinDAO getUsuarioReunionJoinDAO();
}