package cr.ac.ucr.ecci.eseg.estudiaderosucr.comentario.model;

import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.List;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.model.Reunion;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model.Usuario;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "comentario",
        foreignKeys = {
                @ForeignKey(entity = Usuario.class,
                        parentColumns = "id",
                        childColumns = "idUsuario"),
                @ForeignKey( entity = Reunion.class,
                        parentColumns = "id",
                        childColumns = "idReunion",
                        onDelete = CASCADE),
        }
)
public class Comentario {

    @PrimaryKey
    @NonNull
    private String id;

    @ColumnInfo(name = "descripcion")
    private String descripcion;

    @ColumnInfo(name = "fechaComentario")
    private String fechaComentario;

    @ColumnInfo(name = "abilitarMostrarRespuesta")
    private boolean abilitarMostrarRespuesta;

    @ColumnInfo(name = "tieneRespuesta")
    private boolean tieneRespuesta;

    @ColumnInfo(name = "imagenComentario")
    private String imagenComentario;

    private String idReunion;

    private String idUsuario;

    @ColumnInfo(name = "tieneImagen")
    private boolean tieneImagen;

    @Ignore
    private List<Comentario> listaRespuestas;

    public Comentario(String id ,String descripcion, String fechaComentario, String idReunion, String idUsuario) {
        this.id = id;
        this.descripcion = descripcion;
        this.fechaComentario = fechaComentario;
        this.abilitarMostrarRespuesta = false;
        this.idReunion = idReunion;
        this.idUsuario = idUsuario;
        this.tieneRespuesta = false;
        this.tieneImagen = false;
    }

    public Comentario(String id, String descripcion, String fechaComentario, String imagenComentario, Uri uriImagen, String idReunion, String idUsuario) {
        this.id = id;
        this.descripcion = descripcion;
        this.fechaComentario = fechaComentario;
        this.imagenComentario = imagenComentario;
        this.abilitarMostrarRespuesta = false;
        this.idReunion = idReunion;
        this.idUsuario = idUsuario;
        this.tieneRespuesta = false;
        this.tieneImagen = false;
    }

    public Comentario() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaComentario() {
        return fechaComentario;
    }

    public void setFechaComentario(String fechaComentario) {
        this.fechaComentario = fechaComentario;
    }

    public String getImagenComentario() { return imagenComentario; }

    public void setImagenComentario(String imagenComentario) { this.imagenComentario = imagenComentario; }

    public String getIdReunion() {
        return idReunion;
    }

    public void setIdReunion(String idReunion) {
        this.idReunion = idReunion;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public List<Comentario> getListaRespuestas() {
        return listaRespuestas;
    }

    public void setListaRespuestas(List<Comentario> listaRespuestas) {
        this.listaRespuestas = listaRespuestas;
    }

    public boolean isAbilitarMostrarRespuesta() {
        return abilitarMostrarRespuesta;
    }

    public void setAbilitarMostrarRespuesta(boolean abilitarMostrarRespuesta) {
        this.abilitarMostrarRespuesta = abilitarMostrarRespuesta;
    }

    public boolean isTieneRespuesta() {
        return tieneRespuesta;
    }

    public void setTieneRespuesta(boolean tieneRespuesta) {
        this.tieneRespuesta = tieneRespuesta;
    }

    public boolean isTieneImagen() {
        return tieneImagen;
    }

    public void setTieneImagen(boolean tieneImagen) {
        this.tieneImagen = tieneImagen;
    }
}
