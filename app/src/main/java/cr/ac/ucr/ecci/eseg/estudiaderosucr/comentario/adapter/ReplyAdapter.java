package cr.ac.ucr.ecci.eseg.estudiaderosucr.comentario.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.R;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.comentario.model.Comentario;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model.Usuario;

public class ReplyAdapter extends RecyclerView.Adapter<ReplyAdapter.ReplyViewHolder>{

    private List<Comentario> listaRespuestas;
    private CommentAdapter.AdapterCallback mAdapterCallback;
    private CommentAdapter.CommentarioViewHolder mainCommentHolder;


    public ReplyAdapter(List<Comentario> listaRespuestas, CommentAdapter.AdapterCallback callback, CommentAdapter.CommentarioViewHolder mainCommentHolder) {
        this.listaRespuestas = listaRespuestas;

        try {
            this.mAdapterCallback = callback;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AdapterCallback.");
        }

        this.mainCommentHolder = mainCommentHolder;
    }

    @NonNull
    @Override
    public ReplyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View filaRespuesta = LayoutInflater.from(parent.getContext()).inflate(R.layout.reply_layout, parent, false);
        return new ReplyViewHolder(filaRespuesta);
    }

    @Override
    public void onBindViewHolder(@NonNull final ReplyViewHolder holder, final int position) {

        final Comentario comentario = this.listaRespuestas.get(position);
        final boolean tieneImagen = comentario.isTieneImagen();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference reference = database.getReference("usuarios");
        reference.child(comentario.getIdUsuario()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Usuario usuario = dataSnapshot.getValue(Usuario.class);
                holder.imagenCargada.setVisibility(tieneImagen? View.VISIBLE : View.GONE);
                holder.nombre.setText(usuario.getNombre() + " " + usuario.getApellido());
                holder.fecha.setText(comentario.getFechaComentario());
                holder.descripcion.setText(comentario.getDescripcion());

                holder.botonResponder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAdapterCallback.onMethodCallBack(mainCommentHolder.getAdapterPosition());
                    }
                });

                if(tieneImagen) {
                    StorageReference reference = FirebaseStorage.getInstance().getReference();
                    StorageReference image = reference.child(comentario.getImagenComentario());
                    final long ONE_MEGABYTE = 1024 * 1024;
                    image.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                        @Override
                        public void onSuccess(byte[] bytesPrm) { Bitmap bmp = BitmapFactory.decodeByteArray(bytesPrm, 0, bytesPrm.length);
                            holder.imagenCargada.setImageBitmap(bmp);
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return this.listaRespuestas.size();
    }

    public class ReplyViewHolder extends RecyclerView.ViewHolder{

        TextView nombre, fecha, descripcion;
        ImageButton botonBorrar;
        Button botonResponder;
        ImageView imagenCargada;

        public ReplyViewHolder(@NonNull View view) {
            super(view);

            nombre = (TextView) view.findViewById(R.id.reply_username);
            fecha = (TextView) view.findViewById(R.id.reply_date);
            descripcion = (TextView) view.findViewById(R.id.reply_row_content);

            botonBorrar = (ImageButton) view.findViewById(R.id.reply_deleteButton);
            botonResponder = (Button) view.findViewById(R.id.reply_boton_responder);
            imagenCargada = (ImageView) view.findViewById(R.id.image_comment);
        }
    }
}
