package cr.ac.ucr.ecci.eseg.estudiaderosucr.login.ViewModel.data;

import androidx.annotation.Nullable;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.Debug;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.ViewModelData;

/**
 * Identifica el estado de un formulario
 * <p>
 * Estado correcto: almacena una estructura con los valores de los campos del formulario.
 * Estado erroneo: almacena el id de un mensaje de error.
 *
 * @author Daniel Marín M
 */
public class FormState implements ViewModelData<Object> {

    @Nullable
    private Object form;
    @Nullable
    private Integer error;

    public FormState(final Object form) {
        Debug.overlog();
        setData(form);
    }

    public FormState(final Object form, @Nullable final Integer error) {
        Debug.overlog(error);
        setData(form);
        setError(error);
    }

    @Override
    public void setData(@Nullable Object form) {
        Debug.overlog(form);
        this.form = form;
        this.error = null;
    }

    @Override
    public void setError(@Nullable Integer error) {
        Debug.overlog(error);
        this.error = error;
    }

    @Nullable
    @Override
    public Object getData() {
        Debug.overlog(form);
        return form;
    }

    @Nullable
    @Override
    public Integer getError() {
        Debug.overlog(error);
        return error;
    }

    public boolean esValido() {
        boolean valido = (error == null);
        Debug.overlog(valido);
        return valido;
    }


    public static class LoginForm {
        String correo;
        boolean recordar;

        public LoginForm(String correo, boolean recordar) {
            this.correo = correo;
            this.recordar = recordar;
        }

        public String getCorreo() {
            return correo;
        }

        public void setCorreo(String correo) {
            this.correo = correo;
        }

        public boolean isRecordar() {
            return recordar;
        }

        public void setRecordar(boolean recordar) {
            this.recordar = recordar;
        }
    }

    public static class RegisterForm {
        String nombre;
        String correo;
        String telefono;

        public RegisterForm(String nombre, String correo, String telefono) {
            this.nombre = nombre;
            this.correo = correo;
            this.telefono = telefono;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getCorreo() {
            return correo;
        }

        public void setCorreo(String correo) {
            this.correo = correo;
        }

        public String getTelefono() {
            return telefono;
        }

        public void setTelefono(String telefono) {
            this.telefono = telefono;
        }
    }
}