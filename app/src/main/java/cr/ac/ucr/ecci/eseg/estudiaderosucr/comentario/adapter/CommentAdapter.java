package cr.ac.ucr.ecci.eseg.estudiaderosucr.comentario.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.os.HandlerThread;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.UUID;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.R;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.comentario.dao.firebase.ComentarioDAO;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.comentario.model.Comentario;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model.Usuario;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentarioViewHolder> {

    private Context contexto; //contexto de la clase utilizada
    private List<Comentario> listaComentarios; //lista de comentarios que se cargara en el RecyclerView
    private RecyclerView.RecycledViewPool viewPool = new RecyclerView.RecycledViewPool(); //RecyclerView para cargar la lista de comentarios
    private AdapterCallback mAdapterCallback; //adaptador del RecyclerView

    //constructor de la clase
    public CommentAdapter(Context context, List<Comentario> comentarios, AdapterCallback callback){
        this.contexto = context;
        this.listaComentarios = comentarios;

        try {
            this.mAdapterCallback = callback;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AdapterCallback.");
        }
    }

    @NonNull
    @Override
    public CommentarioViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View fila = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_comment, parent, false);
        return new CommentarioViewHolder(fila);
    }

    @Override
    public void onBindViewHolder(@NonNull final CommentarioViewHolder holder, final int position) {
        final Comentario comentario = listaComentarios.get(position);
        final Context context = this.contexto;

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference reference = database.getReference("usuarios").child(comentario.getIdUsuario());

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //atributos del comentario que se cargaran en el RecyclerView
                Usuario usuario = dataSnapshot.getValue(Usuario.class);
                holder.nombre.setText(usuario.getNombre() + " " + usuario.getApellido()); //nombre y apellidos del autor
                holder.fecha.setText(comentario.getFechaComentario()); //fecha
                holder.descripcion.setText(comentario.getDescripcion()); //descripcion

                boolean tieneRespuestas = comentario.isTieneRespuesta(); //permitira saber si un comentario tiene o no respuestas
                boolean abilitarMostrarRespuestas = comentario.isAbilitarMostrarRespuesta(); //habilita el boton para mostrar respuestas de un comentario
                boolean tieneImagen = comentario.isTieneImagen(); //permitira saber si un comentario tiene imagen o no imagen adjunta

                holder.verRespuestas.setVisibility(tieneRespuestas? View.VISIBLE : View.GONE); //se habilita la seccion de respuestas
                holder.imagenCargada.setVisibility(tieneImagen? View.VISIBLE : View.GONE); //se habilita la seccion de imagen del comentario

                //chequeo de respuestas en comentario
                if(abilitarMostrarRespuestas){
                    //ordenar
                    ReplyAdapter adapter = new ReplyAdapter(listaComentarios.get(position).getListaRespuestas(), mAdapterCallback, holder);
                    holder.verRespuestas.setText(R.string.respuestas_off);
                    holder.listaRespuestas.setAdapter(adapter);
                    holder.listaRespuestas.setLayoutManager(new LinearLayoutManager(context));
                    holder.listaRespuestas.setVisibility(View.VISIBLE);


                }else{
                    holder.verRespuestas.setText(R.string.respuestas_on);
                    holder.listaRespuestas.setVisibility(View.GONE);
                }

                //borrado de comentarios
                final ComentarioDAO comentarioDAO = new ComentarioDAO(); //comentario que sera eliminado con un boton
                //se habilitan los botones de borrado solo para los comentarios hechos por el usuario registrado
                FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
                FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                String userId = firebaseUser.getUid(); //id del usuario actual

                if (comentario.getIdUsuario() == userId){ //comprueba si el id del autor del comentario es el mismo que el del usuario actual

                    holder.botonBorrar.setVisibility(View.VISIBLE); //se hace visible el boton para borrar
                    //accion del boton al hacerle click
                    holder.botonBorrar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            comentarioDAO.eliminar(comentario); //se elimina el comentario de la base de datos
                            //se actualiza el recyclerview
                            listaComentarios.remove(position);
                            notifyItemRemoved(position);
                            notifyItemRangeChanged(position, listaComentarios.size());
                        }
                    });
                }

                //carga la imagen del storage de FireBase
                if(tieneImagen) {
                    StorageReference reference = FirebaseStorage.getInstance().getReference();
                    StorageReference image = reference.child(comentario.getImagenComentario());
                    final long ONE_MEGABYTE = 1024 * 1024;
                    image.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                        @Override
                        public void onSuccess(byte[] bytesPrm) { Bitmap bmp = BitmapFactory.decodeByteArray(bytesPrm, 0, bytesPrm.length);
                            holder.imagenCargada.setImageBitmap(bmp);
                        }
                    });
                }

                //boton de responder comentario
                holder.botonResponder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAdapterCallback.onMethodCallBack(position);
                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return listaComentarios.size();
    }

    public class CommentarioViewHolder extends RecyclerView.ViewHolder{
        TextView nombre, fecha, descripcion, verRespuestas;
        ImageButton botonBorrar;
        Button botonResponder;
        RecyclerView listaRespuestas;
        ImageView imagenCargada;

        public CommentarioViewHolder(View view){
            super(view);


            nombre = (TextView) view.findViewById(R.id.comment_username);
            fecha = (TextView) view.findViewById(R.id.comment_date);
            descripcion = (TextView) view.findViewById(R.id.comment_content);
            verRespuestas = (TextView) view.findViewById(R.id.reply_text);

            botonBorrar = (ImageButton) view.findViewById(R.id.deleteButton);
            botonResponder = (Button) view.findViewById(R.id.boton_responder);

            listaRespuestas = (RecyclerView) view.findViewById(R.id.respuestas_comentario);

            verRespuestas.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    listaComentarios.get(getAdapterPosition()).setAbilitarMostrarRespuesta(!listaComentarios.get(getAdapterPosition()).isAbilitarMostrarRespuesta());
                    notifyItemChanged(getAdapterPosition());
                }
            });

            botonBorrar = (ImageButton) view.findViewById(R.id.deleteButton);
            imagenCargada = (ImageView) view.findViewById(R.id.image_comment);
        }
    }

    public interface AdapterCallback{
        void onMethodCallBack(int position);
    }
}