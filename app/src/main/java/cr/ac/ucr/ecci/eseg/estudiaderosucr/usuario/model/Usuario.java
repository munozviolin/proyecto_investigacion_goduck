package cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity(tableName = "usuario",
        indices = {@Index( value = {"correo_institucional"}, unique = true)}
)
public class Usuario implements Parcelable {

    @PrimaryKey
    @NonNull
    private String id = "";

    @ColumnInfo(name = "correo_institucional")
    private String correoInstitucional;

    @ColumnInfo(name = "correo_personal")
    private String correoPersonal;

    @ColumnInfo(name = "nombre")
    private String nombre;

    @ColumnInfo(name = "apellido")
    private String apellido;

    @ColumnInfo(name = "telefono")
    private String telefono;

    @ColumnInfo(name = "sesion_iniciada")
    private boolean sesion_iniciada;

    private String uuid;

    // Constructores

    @Ignore
    public Usuario(){
    }

    @Ignore
    public Usuario(@NonNull String id) {
        this.id = id;
    }

    public Usuario(@NonNull String id, String correoInstitucional, String correoPersonal, String nombre, String apellido, String telefono, boolean sesion_iniciada, String uuid) {
        this.id = id;
        this.correoInstitucional = correoInstitucional;
        this.correoPersonal = correoPersonal;
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
        this.sesion_iniciada = sesion_iniciada;
        this.uuid = uuid;
    }

//     Implementación de los métodos de la interfaz Parceleable

    protected Usuario(Parcel in) {
        id = Objects.requireNonNull(in.readString());
        correoInstitucional = in.readString();
        correoPersonal = in.readString();
        nombre = in.readString();
        apellido = in.readString();
        telefono = in.readString();
        sesion_iniciada = in.readByte() != 0;
        uuid = in.readString();
    }

    public static final Creator<Usuario> CREATOR = new Creator<Usuario>() {
        @Override
        public Usuario createFromParcel(Parcel in) {
            return new Usuario(in);
        }

        @Override
        public Usuario[] newArray(int size) {
            return new Usuario[size];
        }
    };

    @Override
    public int describeContents() {
        // hashCode() of this class
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(correoInstitucional);
        dest.writeString(correoPersonal);
        dest.writeString(nombre);
        dest.writeString(apellido);
        dest.writeString(telefono);
        dest.writeByte((byte) (sesion_iniciada ? 1 : 0));
        dest.writeString(uuid);
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id){
        this.id = id;
    }

    public String getCorreoInstitucional() {
        return correoInstitucional;
    }

    public void setCorreoInstitucional(String correoInstitucional) {
        this.correoInstitucional = correoInstitucional;
    }

    public String getCorreoPersonal() {
        return correoPersonal;
    }

    public void setCorreoPersonal(String correoPersonal) {
        this.correoPersonal = correoPersonal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public boolean isSesion_iniciada() {
        return sesion_iniciada;
    }

    public void setSesion_iniciada(boolean sesion_iniciada) {
        this.sesion_iniciada = sesion_iniciada;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
