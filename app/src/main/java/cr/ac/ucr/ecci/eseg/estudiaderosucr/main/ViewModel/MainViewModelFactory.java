package cr.ac.ucr.ecci.eseg.estudiaderosucr.main.ViewModel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.Debug;

/**
 * Fabrica proveedora de ViewModel para instanciar un LoginViewModel.
 */
public class MainViewModelFactory implements ViewModelProvider.Factory {

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(MainViewModel.class)) {
            Debug.log("creando MainViewModel");
            return (T) new MainViewModel();
        } else {
            Debug.log("MainViewModel no creado");
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}