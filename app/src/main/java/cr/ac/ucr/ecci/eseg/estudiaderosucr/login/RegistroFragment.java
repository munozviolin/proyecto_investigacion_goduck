package cr.ac.ucr.ecci.eseg.estudiaderosucr.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.R;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.login.ViewModel.LoginViewModel;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.login.ViewModel.LoginViewModelFactory;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.login.ViewModel.data.FormState;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.main.MainActivity;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model.Usuario;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.Debug;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.utils.TextChangedListener;

import static cr.ac.ucr.ecci.eseg.estudiaderosucr.login.LoginActivity.CORREO_INSTITUCIONAL;
import static cr.ac.ucr.ecci.eseg.estudiaderosucr.login.LoginActivity.REGISTRO;
import static cr.ac.ucr.ecci.eseg.estudiaderosucr.login.LoginActivity.USUARIO;

public class RegistroFragment extends Fragment {

    private CredentialsSharedPreferences preferences;

    private LoginViewModel viewModel;

    private ProgressBar progressBar;
    private EditText etNombre;
    private EditText etCorreo;
    private EditText etTelefono;
    private Button btnRegistrar;


    static RegistroFragment newInstance() {
        return new RegistroFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Debug.log();

        View view = inflater.inflate(R.layout.fragment_registro_usuario, container, false);
        progressBar = view.findViewById(R.id.pg_frg_login);
        TextView tvNombre = view.findViewById(R.id.tv_frg_register_name);
        etNombre = view.findViewById(R.id.et_frg_register_name);
        TextView tvCorreo = view.findViewById(R.id.tv_frg_register_email);
        etCorreo = view.findViewById(R.id.et_frg_register_email);
        TextView tvTelefono = view.findViewById(R.id.tv_frg_register_phone);
        etTelefono = view.findViewById(R.id.et_frg_register_phone);
        btnRegistrar = view.findViewById(R.id.btn_frg_register);

        // listeners
        setOnClickListeners();
        setOnFocusChangeListener(etNombre);
        setOnFocusChangeListener(etCorreo);
        setOnFocusChangeListener(etTelefono);

        addTextChangedListener(etNombre, tvNombre);
        addTextChangedListener(etCorreo, tvCorreo);
        addTextChangedListener(etTelefono, tvTelefono);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Debug.log();

        LoginViewModelFactory factory = new LoginViewModelFactory();
        ViewModelProvider provider = new ViewModelProvider(requireActivity(), factory);
        viewModel = provider.get(LoginViewModel.class);

        preferences = new CredentialsSharedPreferences(requireContext());

        cargarDatos();

        setObservers();
    }

    private void cargarDatos() {
        Usuario usuario = viewModel.getNuevoUsuario();
        if (usuario == null) {
            String correo = viewModel.getEtCorreoInstitucionalValue();
            if (correo == null) {
                correo = preferences.cargarPreferencia(CORREO_INSTITUCIONAL);
            }
            usuario = viewModel.crearNuevoUsuario(correo);
        }
        Debug.log(usuario);
        String nombre = usuario.getNombre() + " " + usuario.getApellido();
        etNombre.setText(nombre);
    }

    private void setOnClickListeners() {
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                preferences.guardarPreferencia(REGISTRO, null);
                viewModel.registrar();
            }
        });
    }

    private void setOnFocusChangeListener(final EditText editText) {
        Debug.log(editText);
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Debug.overlog(editText.getText());
                viewModel.validarFormulario(etNombre.getText().toString(),
                        etCorreo.getText().toString(), etTelefono.getText().toString());
            }
        });
    }

    private void addTextChangedListener(final EditText editText, final TextView textView) {
        Debug.log(editText);
        editText.addTextChangedListener(new TextChangedListener() {
            @Override
            public void afterTextChanged(Editable s) {
                Debug.overlog(s);
                if (s.toString().isEmpty()) {
                    textView.setVisibility(View.INVISIBLE);
                } else {
                    textView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void setObservers() {
        Debug.log();
        viewModel.getFormState().observe(getViewLifecycleOwner(), new Observer<FormState>() {
            @Override
            public void onChanged(FormState formState) {
                Debug.log("RegisterFormState:", formState);
                if (formState != null) {
                    btnRegistrar.setEnabled(formState.esValido());
                    Integer error = formState.getError();
                    etNombre.setError(null);
                    etCorreo.setError(null);
                    etTelefono.setError(null);
                    if (error != null) {
                        switch (error) {
                            case R.string.invalid_name:
                                etNombre.setError(getString(R.string.et_name_error));
                                break;
                            case R.string.invalid_email:
                                etCorreo.setError(getString(R.string.et_email_error));
                                break;
                            case R.string.invalid_phone:
                                etTelefono.setError(getString(R.string.et_phone_error));
                                break;
                        }
                    }
                }
            }
        });

        viewModel.getUsuario().observe(requireActivity(), new Observer<Usuario>() {
            @Override
            public void onChanged(Usuario usuario) {
                Debug.log("Usuario",usuario);
                if(usuario != null) {
                    Activity activity = requireActivity();
                    Intent intent = new Intent(requireContext(), MainActivity.class);
                    intent.putExtra(USUARIO, usuario);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    activity.setResult(Activity.RESULT_OK);
                    activity.finish();
                }

            }
        });
    }

}
