package cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.R;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.dao.firebase.ReunionesDAO;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.model.Reunion;
import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.repository.UserRepository;

/**
 * A simple {@link Fragment} subclass.
 */
public class CrearReunionFragment extends Fragment implements View.OnClickListener {



    private String latitud;
    private String longitud;
    private String curso;
    private String titulo;
    private String descripcion;
    private String fecha;
    private String hora;
    private String userID;
    private String autor;

    public Button buttonCrearReunion;
    public EditText editTextNombreReunion;
    public EditText editTextDescripcion;

    private UserRepository userRepository;
    private View v;

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public CrearReunionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            //TODO implementacion de user
            setAutor("");
            setUserID(UserRepository.getInstance().getUid());
            setTitulo("");
            setCurso("");
            setDescripcion("");
            setFecha("");
            setHora("");

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                setLatitud(bundle.getString("latitude", ""));
                setLongitud(bundle.getString("longitude", ""));
            }

            v = inflater.inflate(R.layout.fragment_crear_reunion, container, false);
            TextView textViewNombreUsuario = v.findViewById(R.id.textViewNombreUsuario);
            textViewNombreUsuario.setText(autor);

            final AutoCompleteTextView autoCompleteCursosReunion =
                    v.findViewById(R.id.autoCompleteCursosReunion);
            final String[] cursos = getResources().getStringArray(R.array.cursos);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(requireContext(),
                    android.R.layout.simple_list_item_1, cursos);
            autoCompleteCursosReunion.setAdapter(adapter);

            final EditText mDisplayDate;
            mDisplayDate = (EditText) v.findViewById(R.id.editTextFechaReunion);
            mDisplayDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Calendar cal = Calendar.getInstance();
                    int year = cal.get(Calendar.YEAR);
                    int month = cal.get(Calendar.MONTH);
                    int day = cal.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    String fecha = String.valueOf(dayOfMonth) + "/" +
                                            String.valueOf(monthOfYear + 1) + "/" + String.valueOf(year);
                                    mDisplayDate.setText(fecha);
                                    setFecha(fecha);
                                }
                            }, year, month, day);
                    datePicker.show();
                }
            });

            final EditText mDisplayHour = v.findViewById(R.id.editTextHoraReunion);
            mDisplayHour.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Calendar cal = Calendar.getInstance();
                    int hour = cal.get(Calendar.HOUR_OF_DAY);
                    int minute = cal.get(Calendar.MINUTE);
                    TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                            new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker view, int pHour,
                                                      int pMinute) {
                                    String ampmvalor = " am";
                                    int horaFinal = pHour;
                                    String minutoFinal = "" + pMinute;
                                    if (pHour > 12) {
                                        horaFinal -= 12;
                                        ampmvalor = " pm";
                                    } else if (pHour == 0) {
                                        horaFinal = 12;
                                    }

                                    if (pMinute < 10) {
                                        minutoFinal = "0" + pMinute;
                                    }

                                    String hora = horaFinal + ":" + minutoFinal + ampmvalor;
                                    mDisplayHour.setText(hora);
                                    setHora(hora);
                                }
                            }, hour, minute, false);
                    timePickerDialog.show();
                }
            });

        editTextDescripcion = v.findViewById(R.id.editTextDescripcion);
        editTextNombreReunion = v.findViewById(R.id.editTextNombreReunion);
        buttonCrearReunion = v.findViewById(R.id.buttonCrearReunion);

        //se hace click en crear reunión
        buttonCrearReunion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDescripcion(editTextDescripcion.getText().toString());
                setTitulo(editTextNombreReunion.getText().toString());
                setCurso(autoCompleteCursosReunion.getText().toString());
                //Si algún campo está vacío se le solicita al usuario que llene todos los espacios
                if (curso.equals("") || titulo.equals("") || descripcion.equals("") ||
                        hora.equals("") || fecha.equals("")) {
                    Toast.makeText(requireContext(), "Por favor llene todos los espacios",
                            Toast.LENGTH_LONG).show();
                } else {
                    List<String> cursosLista = Arrays.asList(cursos);
                    if (cursosLista.contains(curso)) {
                        abrirDialogo();
                    } else {
                        Toast.makeText(requireContext(), "El curso ingresado no pertenece a la " +
                                        "lista de \ncursos permitidos, por favor elija uno de la lista.",
                                Toast.LENGTH_LONG).show();
                    }
                }


            }
        });
        return v;
    }

    /**
     * Diálogo que se abre para confirmar la reunión
     * Crea la reunión en la base de datos
     */

    public void abrirDialogo() {
        final AlertDialog dialog = new AlertDialog.Builder(requireContext())
                .setTitle("Crear Reunión")
                .setTitle("¿Desea crear esta reunión?")
                .setPositiveButton("Ok", null)
                .setNegativeButton("Cancelar", null)
                .show();

        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Reunion reunion = new Reunion(UUID.randomUUID().toString(), getTitulo(),
                        getDescripcion(), getFecha() + " " + getHora(),
                        Double.parseDouble(getLatitud()), Double.parseDouble(getLongitud()),
                        getUserID(), getAutor(), getCurso());
                ReunionesDAO reunionesDAO = new ReunionesDAO();
                reunionesDAO.agregar(reunion);
                Toast.makeText(requireContext(), "Se agregó la reunión",
                        Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });

    }

    @Override
    public void onClick(View view) {

    }
}
