package cr.ac.ucr.ecci.eseg.estudiaderosucr.reunion.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.util.List;

import cr.ac.ucr.ecci.eseg.estudiaderosucr.usuario.model.Usuario;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "reunion",
        foreignKeys = @ForeignKey(entity = Usuario.class,
                parentColumns = "id",
                childColumns = "idUsuario",
                onDelete = CASCADE)
)

public class Reunion implements Parcelable {

    @PrimaryKey
    @NonNull
    private String id;

    @ColumnInfo(name = "titulo")
    private String titulo;

    @ColumnInfo(name = "descripcion")
    private String descripcion;

    @ColumnInfo(name = "horaInicio")
    private String horaInicio;

    @ColumnInfo(name = "latitud")
    private double latitud;

    @ColumnInfo(name = "longitud")
    private double longitud;

    //esto hay que eliminarlo y utilizar el User ID
    @ColumnInfo(name="autor")
    private String autor;

    @ColumnInfo(name = "curso")
    private String curso;

    //TODO CAMBIAR A TIPO USUARIO
    private String idUsuario;

    public Reunion() {  }

    public Reunion(String id, String titulo, String descripcion, String horaInicio, double latitud, double longitud, String idUsuario, String autor, String curso) {
        this.id = id;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.horaInicio = horaInicio;
        this.latitud = latitud;
        this.longitud = longitud;
        this.idUsuario = idUsuario;
        this.autor = autor;
        this.curso = curso;
    }

    public Reunion(String id, String titulo, String descripcion, String horaInicio, double latitud, double longitud, String idUsuario, List<String> usuarios) {
        this.id = id;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.horaInicio = horaInicio;
        this.latitud = latitud;
        this.longitud = longitud;
        this.idUsuario = idUsuario;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getCurso() { return curso; }

    public void setCurso(String curso) { this.curso = curso; }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
